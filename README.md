# README Captain
Required packages: libjson-perl, libmojolicious-perl, libtie-ixhash-perl, liburi-encode-perl

#####Usage
	./spider.pl --site Forever21 --action saveMap --mapfile FILES/Forever21_map.json
	./spider.pl --site Forever21 --action showCategories --mapfile FILES/Forever21_map.json [--pid 65536]
	./spider.pl --site Forever21 --action saveProds --mapfile FILES/Forever21_map.json  --prodsfile FILES/Forever21_products.json [--url http://product/url]
