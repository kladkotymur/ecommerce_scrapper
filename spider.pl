#!/usr/bin/perl

use strict;
use warnings;

use File::Basename;

use lib dirname(__FILE__) . "/libs/";

use Mojo::UserAgent;
use Module::Load;
use Getopt::Long;

use SitesConfig qw($sitesConfig);
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray);
use Common::UserAgents qw(getRandomAgent);

my $site = "";
my $action = "";
my $prodsFile = "";
my $mapFile = "";
my $help = "";
my $rand = 0;
my $url = "";
my $pid = "";

GetOptions(
	"site=s"      => \$site,
	"action=s"    => \$action,
	"prodsfile=s" => \$prodsFile,
	"mapfile=s"   => \$mapFile,
	"rand=i"      => \$rand,
	"url=s"       => \$url,
	"pid=s"       => \$pid,
	"help"        => \$help
);

sub showHelp
{
	my $error = shift;
	
	print "ERROR: $error\n\n" if ($error);
	print "Usage: ./spider.pl --site site --action action\n"
		. "\t --help      show this help\n"
		. "\t --site     required site code from SitesConfig.pm\n"
		. "\t --action    required valid option saveMap and saveProds\n"
		. "\t     saveMap        - save site map json into mapfile\n"
		. "\t     saveProds      - load site map from mapfile and save products in prodsfile\n"
		. "\t     showCategories - show categories from map file\n"
		. "\t --mapfile   required file to save or load site map\n"
		. "\t --prodsfile required for 'saveProds' action\n"
		. "\t --pid product unique; used with showCategories action\n";
	exit(0);
}

sub validateOptions
{
	if (! exists($sitesConfig->{$site}))
	{
		showHelp("Could not find site '$site' in SitesConfig");
	}

	if (! inArray($action, qw(saveMap saveProds showCategories)))
	{
		showHelp("Invalid action: '$action'; "
			. "should be saveMap or saveProds");
	}

	if (inArray($action, qw(saveMap showCategories)) && !$mapFile)
	{
		showHelp("Required mapfile");
	}

	if ($action eq "saveProds" && !$mapFile)
	{
		showHelp("Required mapfile");
	}

	if ($action eq "saveProds" && !$prodsFile)
	{
		showHelp("Required prodsfile");
	}
}

sub main
{
	showHelp() if ($help);

	validateOptions();

	logit("START handle $site => $action");

	my $siteConfig = $sitesConfig->{$site};
	my $sitePackage = $siteConfig->{"package"};
	my $siteUrl = $siteConfig->{"baseUrl"};

	my $agent = Mojo::UserAgent->new();

	exists($siteConfig->{"agent"}) ?
		$agent->transactor->name( $siteConfig->{"agent"} ) :
		$agent->transactor->name( getRandomAgent() );

	my $handlerPackage = "Sites::${sitePackage}::Handler";
	load($handlerPackage);

	my $handler = $handlerPackage->new(
	 	"baseUrl" => $siteUrl,
	 	"agent" => $agent,
	);

	my %settings;
	$settings{"rand"} = int($rand) if ($rand);
	$settings{"url"} = $url if ($url);
	$settings{"pid"} = $pid if ($pid);

	$handler->saveMap($mapFile) if ($action eq "saveMap");
	$handler->showCategories($mapFile, %settings) if ($action eq "showCategories");
	$handler->saveProducts($mapFile, $prodsFile, %settings) if ($action eq "saveProds");

	logit("END handle $site => $action");
}

eval {
	main();
};

print "ERROR: $@" if ($@);
