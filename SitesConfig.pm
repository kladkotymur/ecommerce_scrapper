package SitesConfig;

use strict;
use warnings;

use File::Basename;
use Exporter;

use base qw(Exporter);

our @EXPORT_OK = qw($sitesConfig LOGS_DIR);

our $sitesConfig = {
	"Forever21" => {
		"package" => "Forever21",
		"baseUrl" => "http://www.forever21.com/CA/"
	},
	"GapCa" => {
		"package" => "GapCa",
		"baseUrl" => "http://www.gapcanada.ca/"
	},
	"AdditionElle" => {
		"package" => "AdditionElle",
		"baseUrl" => "http://www.additionelle.com/"
	},
	"AldoShoes" => {
		"package" => "AldoShoes",
		"baseUrl" => "https://www.aldoshoes.com/"
	},
	"Allsaints" => {
		"package" => "Allsaints",
		"baseUrl" => "https://www.ca.allsaints.com/"
	},
	"AmericanEagle" => {
		"package" => "AmericanEagle",
		"baseUrl" => "https://www.ae.com/"
	},
	"AnnTaylor" => {
		"package" => "AnnTaylor",
		"baseUrl" => "http://www.anntaylor.com/"
	},
	"Anthropologie" => {
		"package" => "Anthropologie",
		"baseUrl" => "https://www.anthropologie.com/"
	},
	"Ardene" => {
		"package" => "Ardene",
		"baseUrl" => "http://www.ardene.com/"
	},
	"Arcteryx" => {
		"package" => "Arcteryx",
		"baseUrl" => "http://www.arcteryx.com/"
	},
	"OldNavy" => {
		"package" => "OldNavy",
		"baseUrl" => "http://oldnavy.gapcanada.ca/"
	},
	"BananaRepublic" => {
		"package" => "BananaRepublic",
		"baseUrl" => "http://bananarepublic.gapcanada.ca/"
	},
	"HandM" => {
		"package" => "HandM",
		"baseUrl" => "http://www2.hm.com/"
	},
	"BeautyBoutique" => {
		"package" => "BeautyBoutique",
		"baseUrl" => "https://www.beautyboutique.ca/"
	},
	"ChildrensPlace" => {
		"package" => "ChildrensPlace",
		"baseUrl" => "http://www.childrensplace.com/"
	},
	"Zara" => {
		"package" => "Zara",
		"baseUrl" => "https://www.zara.com/"
	},
	"DynamiteClothing" => {
		"package" => "DynamiteClothing",
		"baseUrl" => "http://www.dynamiteclothing.com/"
	},
	"CallitSpring" => {
		"package" => "CallitSpring",
		"baseUrl" => "http://www.callitspring.com/"
	},
	"UrbanPlanet" => {
		"package" => "UrbanPlanet",
		"baseUrl" => "http://urban-planet.com/",
		"agent"   => "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:52.0) Gecko/20100101 Firefox/52.0"
	}
};

use constant LOGS_DIR => dirname(__FILE__) . "/LOGS/";

1;
