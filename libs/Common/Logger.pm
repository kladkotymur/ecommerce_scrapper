package Common::Logger;

use strict;
use warnings;

use SitesConfig qw(LOGS_DIR);

use POSIX qw(strftime);
use Exporter;

use base qw(Exporter);

our @EXPORT_OK = qw(debug logit);

sub debug
{
	local $| = 1;
	print "@_\n";
}

sub logit
{
	my $str = shift;

	debug($str);

	my $file = LOGS_DIR . strftime("%Y-%m-%d", localtime()) . ".log";
	my $fh;
	
	open($fh, ">>", $file) || die("Could not open $file : $!\n");
	$str = "[ " . strftime("%H:%M:%S", localtime()) ." ] $str\n";
	print $fh $str;
	close($fh);
}

1;
