package Common::DomUtils;

use strict;
use warnings;

use Exporter;

use base qw(Exporter);

use Common::Utils qw(trim);

our @EXPORT_OK = qw(getCSS getListCSS);

sub getCSS
{
	my ($dom, $selector, $attr, $default) = @_;

	$default ||= "";

	my $res = $default;
	eval {
		if ($attr)
		{
			$res = trim($dom->at($selector)->attr($attr));
		}
		else
		{
			$res = trim($dom->at($selector)->text);
		}
	};

	return $res;
}

sub getListCSS
{
	my ($dom, $selector, $attr, @default) = @_;

	my @res = @default;

	eval {
		if ($attr)
		{
			@res = $dom
				->find($selector)
				->attr($attr)
				->each();
		}
		else
		{
			@res = $dom
				->find($selector)
				->map( sub { $_->text } )
				->each();
		}
	};

	return @res;
}

1;
