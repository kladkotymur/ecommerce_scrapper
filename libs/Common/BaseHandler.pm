package Common::BaseHandler;

use strict;
use warnings;

use Common::Logger qw(logit debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase filterShortPrefix);
use Common::Filters; # qw(isNewArrival isSale isFeature isRegular);
use Common::CategoriesTree;

use List::Util qw(shuffle);
use JSON;
use Data::Dumper;
use Encode qw(encode_utf8);

use constant MAX_REDIRECTS => 10;

sub new
{
	my $class = shift;
	
	my $self = {
		"lastUrl" => "",
		"productFields" => { },
		@_
	};

	return bless($self, $class);
}

sub getBaseUrl
{
	my $self = shift;

	return $self->{"baseUrl"};	
}

sub getAgent
{
	my $self = shift;

	return $self->{"agent"};
}

sub joinUrl
{
	my ($self, $url) = @_;

	return $url =~ /^http/ ?
		$url :
		$self->getBaseUrl() . $url;
}

sub resetProductFields
{
	my $self = shift;

	$self->{"productFields"} = {
		"pid"                  => undef,
		"isNewArrival"         => undef,
		"isSale"               => undef,
		"isFeature"            => undef,
		"isOnlineExclusive"    => undef,
		"parentName"           => "",
		"validUrl"             => undef,
		"mSubCategories"       => [],  # main
		"naSubCategories"      => [],  # new arrivals
		"sSubCategories"       => [],  # sales
		"fSubCategories"       => []   # features
	};
}

sub setProductField
{
	my ($self, $k, $v) = @_;
	$self->{"productFields"}->{$k} = $v;
}

sub addProductField
{
	my ($self, $k, $v) = @_;
	push(@{$self->{"productFields"}->{$k}}, $v);
}

sub getProductField
{
	my ($self, $k) = @_;

	return $self->{"productFields"}->{$k};
}

sub getLastUrl
{
	my ($self) = @_;
	return $self->{"lastUrl"};
}

sub getPage
{
	my ($self, $url, %opts) = @_;

	$self->{"lastUrl"} = $url;

	my $agent = $self->getAgent();

	if (exists($opts{'cookies'}))
	{
		$agent->cookie_jar($opts{'cookies'});
	}

	my $page = $agent->get($url);

	return $page->res if ($page->success);

	my $error = $page->error;

	if (ref($error))
	{
		logit("Could not load $url: " . $error->{"message"}
			. " " . $error->{"code"});
	}
	else
	{
		logit("Could not load $url: $error");
	}

	return undef;
}

sub getRedirectPage
{
	my ($self, $url, %opts) = @_;

	$opts{"redirectsCount"} = 0 if (!exists($opts{"redirectsCount"}));

	$opts{"redirectsCount"} ++;

	my $page = $self->getPage($url, %opts);

	return undef if (!$page);

	if ($opts{"redirectsCount"} > MAX_REDIRECTS)
	{
		debug("A lot of redirects");
		return $page;
	}

	my $redirectUrl;
	if (inArray($page->code, qw(301 302)))
	{
		if (exists($opts{"baseUrl"}))
		{
			$redirectUrl = $opts{"baseUrl"} . $page->headers->location();
		}
		else
		{
			$redirectUrl = $page->headers->location();
		}

		debug("Follow redirect $redirectUrl => "
			. $opts{"redirectsCount"});
		$page = $self->getRedirectPage($redirectUrl, %opts);
		return undef if (!$page);
	}

	return $page;
}

sub getRedirectBasePage
{
	my ($self, $url, %opts) = @_;

	return $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl(), %opts);
}

sub getRedirectBasePageNG
{
	my ($self, $url, %opts) = @_;

	return $self->getRedirectPage($url, %opts) if ($url =~ /^http/);
	return $self->getRedirectBasePage($url, %opts);
}

sub saveMapFile
{
	my ($self, $struct, $file) = @_;

	my $fh;

	open($fh, ">", $file) || $self->die("Could not write to $file: $!");
	print $fh to_json($struct, { "pretty" => 1 });
	close($fh);

	logit("Saved in mapsfile: $file successfully");
}

sub startSaveProducts
{
	my ($self, $file) = @_;
	my $fh;

	open($fh, ">", $file) || $self->die("Could not write to $file: $!");
	print $fh "[\n";
	close($fh);
}

sub saveProduct
{
	my ($self, $struct, $file, $isFirst) = @_;

	my $fh;
	my $comma = $isFirst ? ",\n" : "";

	open($fh, ">>", $file) || $self->die("Could not write to $file: $!");
	print $fh ($comma . to_json($struct, { "utf8" => 1, "pretty" => 1} ));
	close($fh);
}

sub endSaveProducts
{
	my ($self, $file) = @_;
	my $fh;

	open($fh, ">>", $file) || $self->die("Could not write to $file: $!");
	print $fh "\n]\n";
	close($fh);
}

sub getProductsMap
{
	my ($self, $file) = @_;

	my $fh;
	my $str = "";

	open($fh, "<", $file) || $self->die("Could not read from $file: $!");
	while (<$fh>)
	{
		$str .= $_;
	}
	close($fh);

	return decode_json(encode_utf8($str));
}

sub preSaveProducts
{
	return 1;
}

# start actions
sub saveMap
{
	my ($self, $file) = @_;

	my $struct = $self->generateMap();

	$self->saveMapFile($struct, $file);
}

sub saveProducts
{
 	my ($self, $mapFile, $prodsFile, %settings) = @_;

	$self->preSaveProducts();

	my $randCount = 0;
	if (exists($settings{"rand"}))
	{
		$randCount = int($settings{"rand"});
		debug("Handle rand $randCount products by category");
	}
 
 	my $struct = $self->getProductsMap($mapFile);
	my $isFirst = undef;
	my ($subStruct, $url, $product, @urls, @tmpUrls);

	my @uniquePids = ();

 	$self->startSaveProducts($prodsFile);

 	foreach $subStruct (@$struct)
 	{
		@urls = @{$subStruct->{"productsUrls"}};

		if ($randCount)
		{
			@tmpUrls = shuffle(@urls);
			@urls = grep( defined, @tmpUrls[1 .. $randCount] );
		}

 		foreach $url (@urls)
 		{
			if (exists($settings{"url"}))
			{
				next if ($url ne $settings{"url"});
			}

			$self->resetProductFields();

			my $pid = $self->getPid($url);

			if (!$pid)
			{
				logit("Could not extract pid from $url");
				next;
			}

			my $categoriesInfo = $self->getCategoriesInfo($pid, $struct);
			if (inArray($pid, @uniquePids))
			{
				debug("Pid $pid has been already handled");
				next;
			}

			push(@uniquePids, $pid);
			$self->setProductField("pid", $pid);

			my $validUrl = $self->getProductField("validUrl");
			$url = $validUrl if ($validUrl);

 			debug("Handling $url ...");

			$product = $self->getProductStruct($url, $struct, $subStruct, $categoriesInfo);

			next if (!$product);

			if (!$product->{"Title"})
			{
				debug("ERROR: Invalid product without title");
				next;
			}

			if (!$product->{"Price"})
			{
				debug("ERROR: Invalid product without Price");
				next;
			}
 
 			$self->saveProduct($product, $prodsFile, $isFirst);
 
 			$isFirst = 1;
 		}
 	}
 
 	$self->endSaveProducts($prodsFile);
}

sub showCategories
{
 	my ($self, $mapFile, %settings) = @_;
 	my $struct = $self->getProductsMap($mapFile);

	my ($subStruct, @urls, $pattern);
	my $tree = Common::CategoriesTree->new();

	my $pid = exists($settings{"pid"}) ? $settings{"pid"} : undef;

	$pattern = $self->getPidPattern($pid) if ($pid);

	foreach $subStruct (@$struct)
	{
		
		if ($pid)
		{
			@urls = grep(/$pattern/, @{$subStruct->{"productsUrls"}});
			next if (! @urls);
			debug(join("\n", @urls));
		}

		$tree->addNode($subStruct);
	}

	debug();
	debug();
	$tree->show();
}

# end actions

sub die
{
	my ($self, $err) = @_;
	die($err);
}

# product utils

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	$self->die("Please override getProductStruct");
}

sub getPid
{
	my ($self, $url) = @_;

	$self->die("Please override getPid");
}

sub getPidPattern
{
	my ($self, $pid) = @_;

	$self->die("Please override getPidPattern");
}

sub getCategoriesInfo
{
	my ($self, $pid, $struct) = @_;
	my $pattern = $self->getPidPattern($pid);

	my $subStruct;
	my (@info) = ();

	foreach $subStruct (@$struct)
	{
		my @urls = grep(/$pattern/, @{$subStruct->{"productsUrls"}});
		next if (! @urls);

		my $info = { "productsUrls" => [ @urls ] };

		while (my($k, $v) = each(%$subStruct))
		{
			next if ($k eq "productsUrls" || ref($v) eq ref([]));
			$info->{$k} = $v;
		}

		$self->categoriesCallback($info);

		push(@info, $info);
	}

	# debug(Dumper(\@info));

	return [ @info ];
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	$self->die("Please override categoriesCallback");
}

sub isRegular
{
	my ($self, @fields) = @_;
	return Common::Filters::isRegular(@fields);
}

sub isNewArrival
{
	my ($self, @fields) = @_;
	return Common::Filters::isNewArrival(@fields);
}

sub isFeature
{
	my ($self, @fields) = @_;
	return Common::Filters::isFeature(@fields);
}

sub isSale
{
	my ($self, @fields) = @_;
	return Common::Filters::isSale(@fields);
}

sub getSubCategoriesByType
{
	my ($self, $type) = @_;

	my @subCategories = @{$self->getProductField($type)};
	@subCategories = removeDuplicatesIgnoreCase(@subCategories);

	my $subCategories = filterShortPrefix(\@subCategories);
	return @$subCategories;
}

sub getMainSubCategoriesRef
{
	my ($self) = @_;

	return (scalar($self->getSubCategoriesByType("mSubCategories")) > 0) ?
		[ $self->getSubCategoriesByType("mSubCategories") ] : undef;
}

sub getNewArrivalSubCategoriesRef
{
	my ($self) = @_;

	return (scalar($self->getSubCategoriesByType("naSubCategories")) > 0) ?
		[ $self->getSubCategoriesByType("naSubCategories") ] : undef;
}

sub getFeatureSubCategoriesRef
{
	my ($self) = @_;

	return (scalar($self->getSubCategoriesByType("fSubCategories")) > 0) ?
		[ $self->getSubCategoriesByType("fSubCategories") ] : undef;
}

sub getSaleSubCategoriesRef
{
	my ($self) = @_;

	return (scalar($self->getSubCategoriesByType("sSubCategories")) > 0) ?
		[ $self->getSubCategoriesByType("sSubCategories") ] : undef;
}

sub isNewArrivalJSON
{
	my ($self) = @_;

 	return $self->getProductField("isNewArrival") ?  JSON::true : JSON::false;
}

sub isFeatureJSON
{
	my ($self) = @_;

 	return $self->getProductField("isFeature") ?  JSON::true : JSON::false;
}

sub getNonRegularCategorieis
{
	my ($self) = @_;

	return ($self->getNewArrivalCategories(),
		$self->getFeatureCategories(),
		$self->getSaleCategories());
}

sub getCategoriesSeparator
{
	return ",";
}

sub buildCategoriesLine
{
	my ($self, @levels) = @_;

	my @result = ();
	my $level;
	my $separator = $self->getCategoriesSeparator();

	foreach $level (@levels)
	{
		push(@result, $level) if ($level);
	}

	return join($separator, @result);
}

1;
