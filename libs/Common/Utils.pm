package Common::Utils;

use strict;
use warnings;

use Exporter;

use base qw(Exporter);

our @EXPORT_OK = qw(removeDuplicates removeDuplicatesIgnoreCase
	inArray trim filterShortPrefix isArray);

sub removeDuplicates
{
	my (@list) = @_;
	
	my (@newList, $item);

	foreach $item (@list)
	{
		if (!inArray($item, @newList))
		{
			push(@newList, $item);
		}
	}
	
	return @newList;
}

sub removeDuplicatesIgnoreCase
{
	my (@list) = @_;
	
	my (@newList, $item);

	foreach $item (@list)
	{
		my @tmpList = map(lc, @newList);

		if (!inArray(lc($item), @tmpList))
		{
			push(@newList, $item);
		}
	}
	
	return @newList;
}

sub inArray
{
	my ($target, @list) = @_;

	return undef if (!$target);
	return undef if (!@list);

	my $item;

	foreach $item (@list)
	{
		return 1 if ($item eq $target);
	}

	return undef;
}

sub trim
{
	my $str = shift;
	$str =~ s/^\s+|\s+$//g;
	return $str;
}

sub filterShortPrefix
{
	my ($list, $separator) = @_;

	my @list = @$list;
	$separator ||= ",";

	my $cnt = scalar(@list);
	return [] if ($cnt == 0);

	my (@delIndexes) = ();
	my ($i, $j, $item, $item2);

	for ($i = 0; $i < $cnt; $i++)
	{
		$item = $list[$i];

		for ($j = 0; $j < $cnt; $j++)
		{
			$item2 = $list[$j];	

			if (index($item2, $item . $separator) == 0)
			{
				push(@delIndexes, $i);
			}
		}
	}

	foreach $i (@delIndexes)
	{
		delete $list[$i];
	}
	
	@list = grep(defined, @list);

	return \@list;
}

sub isArray
{
	my $list = shift;
	return (ref($list) eq ref([]));
}

1;
