package Common::CategoriesTree;

use strict;
use warnings;

use Data::Dumper;
use Common::Logger qw(debug);

sub new
{
	my ($class, $min) = @_;

	my $self = {
		"tree" => []
	};

	return bless($self, $class);
}

sub addNode
{
	my ($self, $node) = @_;	

	my ($current, $level, $k, $v, $child, $found, $len);

	foreach my $level (1 .. 10)
	{
		$k = "level${level}";
		next if (! exists($node->{$k}));

		$v = $node->{$k};
		next if (!$v);
		$current = $self->{"tree"} if (! $current);

		if (scalar(@$current) == 0)
		{
			push(@$current, {"name" => $v, "children" => []});
			$current = $current->[0]->{"children"};
			next;
		}

		$found = undef;
		foreach $child (@$current)
		{
			if ($v eq $child->{"name"})
			{
				$current = $child->{"children"};
				$found = 1;
				last;
			}
		}

		if (!$found)
		{
			push(@$current, {"name" => $v, "children" => []});
			$len = scalar(@$current);
			$current = $current->[ $len - 1 ]->{"children"};
		}
	}
}

sub show
{
	my ($self, $children, $level) = @_;

	$level ||= 0;
	return if ($level > 10);

	my $prefix = "...." x $level;
	$children = $self->{"tree"} if (!$children);

	my $child;
	
	foreach $child (@$children)
	{
		debug($prefix . $child->{"name"});
		next if (scalar(@{$child->{"children"}}) == 0);

		$self->show($child->{"children"}, $level + 1);
	}
}


1;
