package Common::Filters;

use strict;
use warnings;

use base qw(Exporter);

our @EXPORT_OK = qw(isNewArrival isSale isFeature isRegular filterPrice);

sub isNewArrival
{
	return scalar(grep(/New.Arrival/i, @_));
}

sub isSale
{
	return scalar(grep(/Sale/i, @_));
}

sub isFeature
{
	return scalar(grep(/Feature/i, @_));
}

sub isRegular
{
	return undef if (isNewArrival(@_));
	return undef if (isSale(@_));
	return undef if (isFeature(@_));
	return undef if (grep(/categories/i, @_));

	return 1;
}

sub filterPrice
{
	my ($str) = @_;

	my ($price) = $str =~ /\$([\d.]+)$/;

	return $price if ($price);
	return $str;
}

1;
