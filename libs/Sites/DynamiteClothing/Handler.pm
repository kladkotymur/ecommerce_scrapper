package Sites::DynamiteClothing::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim);
use Common::DomUtils qw(getCSS getListCSS);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

my $cookies;

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("#mainCategoryMenu .categoryMenuItem")
		->map( sub {
			my $el = $_;
			my $level1 = $el->at(".categoryMenuItemSpan a")->attr("title");
			my $level1Url = $self->joinUrl( $el->at(".categoryMenuItemSpan a")->attr("href") );

			$level1Url =~ s/;.*//;

			return $el->find(".subcategoryMenu li a")->map( sub {
				my $level2 = $_->attr("title");
				my $level2Url = $self->joinUrl($_->attr("href"));

				$level2Url =~ s/;.*//;

				return {
					"level1" => $level1,
					"level1Url" => $level1Url,
					"level2" => $level2,
					"level2Url" => $level2Url
				};
			})
			->each();
		})
		->each();
}

sub getSubcategories
{
	my ($self, $url) = @_;

	my $page = $self->getRedirectBasePageNG($url, "cookies" => $cookies);
	return () if (!$page);

	my @result = ();
	eval {
		@result = $page->dom
			->find("#categoryView .subclassCategories li.categoryListingItem a")
			->map( sub {
				my $href = $_->attr("href") || "";
				my $level3Url = $self->joinUrl($href);
				$level3Url =~ s/;.*//;
				return {
					"level3" => trim($_->text),
					"level3Url" => $level3Url
				};
			})
			->each();
	};

	return @result;
}

sub getProductUrls
{
	my ($self, $url) = @_;

	my @urls = ();

	my ($pageUrl, $pageNum, $page, @tmpUrls);
	foreach my $pageNum ( 1 .. 1000)
	{
		$pageUrl = $url . "?page=$pageNum";
		debug("Handling page $pageNum => $pageUrl");

		$page = $self->getRedirectBasePageNG($pageUrl, "cookies" => $cookies);
		last if (!$page);

		@tmpUrls = ();
		eval {
			@tmpUrls = $page->dom
				->find("#main .prodListingImg a")
				->map( sub {
					my $href = $_->attr("href") || "";
					my $url = $self->joinUrl($href);
					$url =~ s/;.*//;
					return $url;
				})
				->each();
		};

		last if (scalar(@tmpUrls) == 0);
		push(@urls, @tmpUrls);
	}

	return @urls;
}

# end links methods

# products methods
# end products methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl() . "ca/";
	my $page = $self->getRedirectBasePageNG($coreUrl);

	$self->die("Could not load $coreUrl") if (!$coreUrl);
	$cookies = $self->getAgent()->cookie_jar;

	my @categories = $self->getParentCategories($page);

	my $category;
	foreach $category (@categories)
	{
		my $categoryUrl = $category->{"level2Url"};
		debug("Handling $categoryUrl ...");

		my @subCategories = (
			{
				"level3"    => "",
				"level3Url" => $categoryUrl
			}
		);

		push(@subCategories, $self->getSubcategories($categoryUrl));

		my $subCategory;
		foreach $subCategory (@subCategories)
		{
			my @productUrls = $self->getProductUrls($subCategory->{"level3Url"});

			if (scalar(@productUrls) == 0)
			{
				debug("No products on " . $subCategory->{"level3Url"});
				next;
			}

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $category->{"level2"},
				"level3"       => $subCategory->{"level3"},
				"productsUrl"  => $subCategory->{"level3Url"},
				"productsCnt"  => scalar(@productUrls),
				"productsUrls" => [ @productUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub preSaveProducts
{
	my ($self) = @_;

	my $page = $self->getRedirectPage($self->getBaseUrl() . "ca/");
	$cookies = $self->getAgent()->cookie_jar;
}

sub getPid
{
	my ($self, $url) = @_;

	my $pid = "";

	if ($url =~ /prdId=/)
	{
		($pid) = $url =~ /prdId=(\w+)$/;
	}
	else
	{
		($pid) = $url =~ /\/(\w+)$/;
	}

	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3);	

	if (lc($level1) eq "new")
	{
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $fullLine);
	}
	elsif (lc($level1) eq "sale")
	{
 		$self->addProductField("sSubCategories", $fullLine);
	}

	if (inArray("online exclusives", map(lc, $level1, $level2, $level3)))
	{
		$self->setProductField("isOnlineExclusive", 1);
	}

	if (!inArray(lc($level1), qw(new sale)))
	{
		$self->setProductField("parentName", $level1)
			if (!$self->getProductField("parentName"));
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}


	$self->addProductField("mSubCategories", $fullLine);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page;

	for my $i (1 .. 4)
	{
		$page = $self->getRedirectBasePageNG($url, "cookies" => $cookies);

		last if ($page);

		debug("Attempt $i => Error load product $url");
		sleep($i);
	}

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my $pid = $self->getProductField("pid");
	my $dom = $page->dom;
	my $title = getCSS($dom, "#prodDetailInfoHeader h1.prodName");
	my @images = map { $_ =~ /^http/ ? $_ : "http:$_" }
		getListCSS($dom, "#additionalViewsPDP a.pdpImageSelector", "href");
	my $description = getCSS($dom, "#descTab0Content");
	my @colors = getListCSS($dom, "#prodDetailSwatch img", "alt");
	my $contentAndCare = join("\n", getListCSS($dom, "#descTab1Content li"));
	my $sizeAndFit = join("\n", getListCSS($dom, "#descTab2Content li"));
	my @sizes = getListCSS($dom, "#productSizes .size:not(.unavailable)");
	my $sizeChart = getCSS($dom, ".sizeChart a", "href");
	$sizeChart =~ s/;.*//;
	my $sizingInfo = $sizeChart ? $self->joinUrl($sizeChart) : "";	

	my $priceSelector = "#prodDetailInfoHeader h2.prodPricePDP";
	my $realPrice = filterPrice(getCSS($dom, "$priceSelector .salePrice"));
	my $salePrice = filterPrice(getCSS($dom, "$priceSelector .withSale"));
	my $nonSalePrice = filterPrice(getCSS($dom, $priceSelector));

	my $price = $realPrice ? $realPrice : $nonSalePrice;
	$salePrice = $salePrice ? $salePrice : undef;
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

	my $isOnlineExclusive = $self->getProductField("isOnlineExclusive") ?
		JSON::true :
		JSON::false;
	
	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "Dynamite",
 		"Title"                  => $title,
 		"Brand"                  => "Dynamite",
 		"Images"                 => [ @images ],
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $description,
 		"ContentAndCare"         => $contentAndCare,
 		"SizeAndFit"             => $sizeAndFit,
 		"Sku"                    => undef,
 		"ProductId"              => $pid,
 		"Sizes"                  => [ @sizes ],
 		"Colours"                => [ @colors ],
 		"Quantity"               => 0,
 		"SizingInfo"             => $sizingInfo,
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => $isOnlineExclusive,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
