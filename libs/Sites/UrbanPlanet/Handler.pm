package Sites::UrbanPlanet::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray isArray removeDuplicatesIgnoreCase trim);
use Common::DomUtils qw(getCSS getListCSS);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods
sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("#nav > li > a")
		->map( sub {
			my $level1 = $_->at("span")->text;
			my $level1Url = $self->joinUrl($_->attr("href"));

			my $levels2 = $_->parent->find(".level0 li.level1 > a");
			if ($levels2->size > 0 )
			{
				return $levels2->map( sub {
					my $level2 = $_->at("span")->text;
					my $level2Url = $self->joinUrl($_->attr("href"));

					my $levels3 = $_->parent->find("li.level2 > a");
					if ($levels3->size > 0 )
					{
						return $levels3->map( sub {
							return {
								"level1" => $level1,
								"level1Url" => $level1Url,
								"level2" => $level2,
								"level2Url" => $level2Url,
								"level3" => $_->at("span")->text,
								"level3Url" => $self->joinUrl($_->attr("href"))
							};
						})
						->each();
					}
					else
					{
						return {
							"level1" => $level1,
							"level1Url" => $level1Url,
							"level2" => $level2,
							"level2Url" => $level2Url,
							"level3" => "",
							"level3Url" => $level2Url
						};
					}
				})
				->each();
			}
			else
			{
			 	return {
			 		"level1" => $level1,
			 		"level1Url" => $level1Url,
			 		"level2" => "",
			 		"level2Url" => $level1Url,
			 		"level3" => "",
			 		"level3Url" => $level1Url
			 	};
			}
		})
		->each();
}

sub getSubcategories
{
	my ($self, $url) = @_;
	my $page = $self->getRedirectPage($url);
	return () if (!$page);

	my @result = ();

	eval {
		@result = $page->dom
			->find(".above-layered-nav li.child-cat a")
			->map( sub {
				return {
					"level4" => $_->text,
					"level4Url" => $self->joinUrl($_->attr("href"))
				};
			})
			->each();
	};

	return @result;
}

sub getProductUrls
{
	my ($self, $url) = @_;

	my @urls = ();
	my $limit = 96;

	foreach my $i (1 .. 1000)
	{
		my $pageUrl = "$url?limit=$limit&p=$i";
		debug("Handling page => $i => $pageUrl");

		my $page = $self->getRedirectPage($pageUrl);
		last if (!$page);

		my @subUrls = ();
		eval {
			@subUrls = $page->dom
				->find(".category-products a.product-image")
				->map(sub {
					return $self->joinUrl($_->attr("href"));
				})
				->each();
		};

		last if (scalar(@subUrls) == 0);
		push(@urls, @subUrls);
	}
	return @urls;
}
# end links methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $page = $self->getRedirectPage($self->getBaseUrl());

	$self->die("Could not load coreUrl") if (!$page);

	my @categories = $self->getParentCategories($page);

	my $category;
	foreach $category (@categories)
	{
		next if (lc($category->{"level1"}) eq "new");

		my $categoryUrl = $category->{"level3Url"};
		debug("Handling $categoryUrl => $categoryUrl ...");
		my @subCategories = (
			{
				"level4"    => "",
				"level4Url" => $categoryUrl
			}
		);

		push(@subCategories, $self->getSubcategories($categoryUrl));
		my $subCategory;
		foreach $subCategory (@subCategories)
		{
			my @productUrls = $self->getProductUrls($subCategory->{"level4Url"});

			if (scalar(@productUrls) == 0)
			{
				debug("No products on " . $subCategory->{"level3Url"});
				next;
			}

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $category->{"level2"},
				"level3"       => $category->{"level3"},
				"level4"       => $subCategory->{"level4"},
				"productsUrl"  => $subCategory->{"level4Url"},
				"productsCnt"  => scalar(@productUrls),
				"productsUrls" => [ @productUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getProductInfo
{
	my ($self, $page) = @_;

	my ($info) = $page->body =~ /new Product\.Config\((.+?)\);/sm;
	my $struct = {};
	eval {
		$struct = decode_json($info);
	};

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\/([\w\-]+)\.html$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid\.html$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3);	

	if (lc($level1) eq "sale")
	{
 		$self->addProductField("sSubCategories", $fullLine);
	}
	else
	{
		$self->setProductField("parentName", $level1)
			if (!$self->getProductField("parentName"));
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}

	$self->addProductField("mSubCategories", $fullLine);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectPage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $info = $self->getProductInfo($page);

	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my $pid = $self->getProductField("pid");
	my $dom = $page->dom;
	my $title = getCSS($dom, ".product-name h1");
	my @images = getListCSS($dom, ".slides a", "href");
	my $description = getCSS($dom, ".tab-content .std");
	my $contentAndCare = join("\n", getListCSS($dom, ".tab-content .std li"));
	my $sku = getCSS($dom, ".product-ids");
	$sku =~ s/SKU# //;

	my $getInfo = sub {
		my ($info, $key) = @_;

		return () if (! exists($info->{"attributes"}->{$key}->{"options"}) );

		my $options = $info->{"attributes"}->{$key}->{"options"};
		return map { $_->{"label"} } @$options;
	};

	my @colors = $getInfo->($info, "284");
	my @sizes = $getInfo->($info, "286");

	my $realPrice = filterPrice(getCSS($dom, ".regular-price .price"));
	my $nonSalePrice = filterPrice(getCSS($dom, ".old-price .price"));
	my $salePrice = filterPrice(getCSS($dom, ".special-price .price"));
	my $price = $realPrice ? $realPrice : $nonSalePrice;
	$salePrice = $salePrice ? $salePrice : undef;
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "URBAN PLANET",
 		"Title"                  => $title,
 		"Brand"                  => "URBAN PLANET",
 		"Images"                 => [ @images ],
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $description,
 		"ContentAndCare"         => $contentAndCare,
 		"SizeAndFit"             => "",
 		"Sku"                    => $sku,
 		"ProductId"              => $pid,
 		"Sizes"                  => [ @sizes ],
 		"Colours"                => [ @colors ],
 		"Quantity"               => 0,
 		"SizingInfo"             => "http://urban-planet.com/size-charts",
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => JSON::false,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
