package Sites::BeautyBoutique::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("#desktop-menu > ul > li.nav-link-2.has-menu")
		->map( sub {
			my $el = $_;
			my $level1 = $_->attr("aria-label");

			if (lc($level1) eq "brands")
			{
				return $el->find(".nav-content li.link a")
					->grep( sub { $_->parent->attr("class") !~ /indexer/ })
					->map( sub {
						return {
							"level1" => $level1,
							"level2" => $_->text,
							"level3" => "",
							"level3Url" => $self->joinUrl($_->attr("href"))
						};
					})
					->each();
			}
			else
			{
				return $el->find(".menu-flyout .nav-column > a")
					->map( sub {
						my $level2 = $_->text;
						my $level2Url = $self->joinUrl($_->attr("href"));

						my $level3Links = $_->parent
							->find(".content-links .link a");

						if ($level3Links->size > 0)
						{
							return $level3Links->map( sub {
								return {
									"level1" => $level1,
									"level2" => $level2,
									"level3" => $_->text,
									"level3Url" => $self->joinUrl($_->attr("href"))
								};
							})
							->each();
						}

						return {
							"level1" => $level1,
							"level2" => $_->text,
							"level3" => "",
							"level3Url" => $self->joinUrl($_->attr("href"))
						};
					})
					->each();
			}
		})
		->each();
}

sub getProductUrls
{
	my ($self, $url) = @_;

	my $page = $self->getRedirectBasePage($url);

	return () if (!$page);

	my $shopAllUrl;
	eval {
		my $href = $page->dom
			->at(".shop-all-bottom a")->attr("href");
		$shopAllUrl = $self->joinUrl($href);
	};

	if ($shopAllUrl)
	{
		debug("Go to Shop All Url: $shopAllUrl");
		$page = $self->getRedirectBasePage($shopAllUrl);
		return () if (!$page);
	}

	my @productUrls = ();

	my $getPagesInfo = sub {
		my $page = shift;
		my @pagesInfo = ();
		eval {
			@pagesInfo = $page->dom->find(".pagination-nav li.active a")
				->map( sub {
					return {
						"url" => $self->joinUrl($_->attr("href")),
						"num" =>trim($_->text)
					};
				})
				->each();
		};
		return @pagesInfo;
	};

	my $getUrls = sub {
		my $page = shift;
		my (@urls) = ();
		eval {
			@urls = $page->dom->find(".plp-product-tiles .product-brand-name a")
				->map( sub { $self->joinUrl($_->attr("href")) } )
				->each();
		};
		return @urls;
	};

	push(@productUrls, $getUrls->($page));

	my $i = 0;
	my $maxPages = 1000;
	my @visitedPages = (1);
	my $info;
	while (1)
	{
		last if (!$page);
		my @pagesInfo = $getPagesInfo->($page);

		$page = undef;
		while ($info = shift(@pagesInfo))
		{
			my $num = $info->{"num"};
			next if inArray($num, @visitedPages);

			push(@visitedPages, $num);
			$page = $self->getRedirectBasePage($info->{"url"});
			last if (!$page);

			debug("Handling page: $num " . $info->{"url"});
			push(@productUrls, $getUrls->($page));
		}

		$i++;
		last if ($i >= $maxPages);
	}

	return @productUrls;
}

# end links methods

# products methods

sub getProductInfo
{
	my ($self, $pid) = @_;

	my $url = $self->getBaseUrl() . "/p/variant?productCode=$pid&qty=1";
	my $page = $self->getRedirectBasePage($url);

	return undef if (!$page);

	my $struct;
	eval {
		$struct = decode_json($page->body);
	};

	return $struct;
}

sub getTitle
{
	my ($self, $info) = @_;

	return exists($info->{"name"}) ?  $info->{"name"} : "";
}

sub getBrand
{
	my ($self, $info) = @_;
	return exists($info->{"brandName"}) ?  $info->{"brandName"} : "";
}

sub getDescription
{
	my ($self, $info) = @_;
	return exists($info->{"description"}) ?  $info->{"description"} : "";
}

sub getContentAndCare
{
	my ($self, $info) = @_;
	my $ingridients = exists($info->{"ingredients"}) ?  $info->{"ingredients"} : "";
	my $apply =  exists($info->{"howToApply"}) ?  $info->{"howToApply"} : "";

	return trim(join("\n", $ingridients, $apply));
}

sub getPrices
{
	my ($self, $info) = @_;

	my $price;
	my $salePrice;

	if (exists($info->{"regularPriceValue"}))
	{
		$price = filterPrice($info->{"regularPriceValue"});
	}
	$price = sprintf("%.2f", $price) if ($price);

	if (exists($info->{"specialPrice"}))
	{
		$salePrice = filterPrice($info->{"specialPrice"});
	}
	$salePrice = sprintf("%.2f", $salePrice) if ($salePrice);

	$salePrice = undef if ($price eq $salePrice);

	return ($price, $salePrice);
}

sub getColors
{
	my ($page, $info, $pid) = @_;

	my @colors = ();

	my $variant;
	foreach $variant (@{$info->{"variantOptions"}})
	{
		push(@colors, $variant->{"name"}) if ($variant->{"code"} eq $pid);
	}
	@colors = grep(defined, @colors);

	return \@colors;
}

sub getImages
{
	my ($self, $info) = @_;

	my (@images) = ();
	my @zoom = ();

	if (exists($info->{"productImages"}->{"zoomimages"}))
	{
		@zoom = @{$info->{"productImages"}->{"zoomimages"}};
	}

	my $img;
	foreach $img (@zoom)
	{
		push (@images, $img->{"large"}) if (exists($img->{"large"}));
	}

	@images = grep(defined, @images);

	return \@images;
}

# end products methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl();

	my $page = $self->getRedirectBasePage($coreUrl);

	$self->die("Could not load $coreUrl") if (!$coreUrl);

	my @categories = $self->getParentCategories($page);

	my ($category);

	foreach $category (@categories)
	{
		my $categoryUrl = $category->{"level3Url"};
		debug("Handling $categoryUrl ...");

		my @productUrls = $self->getProductUrls($categoryUrl);
		if (scalar(@productUrls) == 0)
		{
			debug("No products");
			next;
		}

		my $subStruct = {
			"level1"       => $category->{"level1"},
			"level2"       => $category->{"level2"},
			"level3"       => $category->{"level3"},
			"productsUrl"  => $category->{"level3Url"},
			"productsCnt"  => scalar(@productUrls),
			"productsUrls" => [ @productUrls ]
		};
		push(@$struct, $subStruct);
	}
	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /variantCode=(\d+)$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/variantCode=$pid$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	return if (lc($level1) eq "brands");

	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3);
	my $line = $self->buildCategoriesLine($level2, $level3);

	if (lc($level1) =~ /sale/)
	{
 		$self->addProductField("sSubCategories", $fullLine) if ($fullLine);
 		$self->addProductField("mSubCategories", $fullLine) if ($fullLine);
	}
	elsif (lc($level2) =~ /sale/)
	{
 		$self->addProductField("sSubCategories", $line) if ($line);
 		$self->addProductField("mSubCategories", $line) if ($line);
	}
	else
	{
		$self->setProductField("parentName", $level1) if (!$self->getProductField("parentName"));
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
		$self->addProductField("mSubCategories", $line)
			if ($self->getProductField("parentName") eq $level1 && $line);
	}
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectBasePage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $pid = $self->getProductField("pid");
	my $productInfo = $self->getProductInfo($pid);

	if (!$productInfo)
	{
		debug("Error: could not get product info");
		return undef;
	}


	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my ($price, $salePrice) = $self->getPrices($productInfo);
 	my $sale = ($salePrice && $salePrice < $price) ? JSON::true : JSON::false;

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "Beauty Boutique",
 		"Title"                  => $self->getTitle($productInfo),
 		"Brand"                  => $self->getBrand($productInfo),
 		"Images"                 => $self->getImages($productInfo),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $self->getDescription($productInfo),
 		"ContentAndCare"         => $self->getContentAndCare($productInfo),
 		"SizeAndFit"             => undef,
 		"Sku"                    => undef,
 		"ProductId"              => $pid,
 		"Sizes"                  => [], # $self->getSizes($page, $pid),
 		"Colours"                => [], # $self->getColors($productInfo, $pid),
 		"Quantity"               => 0,
 		"SizingInfo"             => undef,
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
