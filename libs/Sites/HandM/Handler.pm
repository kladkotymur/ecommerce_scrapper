package Sites::HandM::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("nav.primary-menu > ul > li")
		->map(sub {
			my $el = $_;
			my $level1 = $el->at("a")->text;

			my $level2Els = $el->find("h4");

			return undef if (!$level2Els);

			return $level2Els->map(sub {
				my $subEl = $_;
				my $level2 = $subEl->text;

				return $subEl->parent->find("li")
					->map(sub {
						my $li = $_;
						my $level3 = trim($li->at("a")->text);
						my $url = $li->at("a")->attr("href");

						my $level3Url = $url =~ /^http/ ?
							$url :
							$self->getBaseUrl() . $url;

						return {
							"level1" => $level1,
							"level2" => $level2,
							"level3" => $level3,
							"level3Url" => $level3Url
						};
					})
					->each();	
			})
			->each();
		})
		->each();
}

sub getSubCategories
{
	my ($self, $page) = @_;
	my @subCategories = ();

	eval {
		my $currentCategory = $page->dom
			->at(".section-menu-category li.section-menu-subdepartment.current");
		@subCategories = $currentCategory->find("li a")
			->map( sub {
				my $level4 = trim($_->text);
				my $url = $_->attr("href");
				my $level4Url = $url =~ /^http/ ?
					$url :
					$self->getBaseUrl() . $url;
				return {
					"level4" => $level4,
					"level4Url" => $level4Url
				};
			})
			->each();
	};

	return @subCategories;
}

sub getProductsUrls
{
	my ($self, $url) = @_;

	my $maxCnt = 10000; # ugly hardcode :(
	$url = $url . "?sort=stock&offset=0&page-size=$maxCnt";

	my $page = $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl());

	if (!$page)
	{
		debug("Could not get pages from $url");
		return ();
	}

	my @urls = ();
	eval {
		@urls = $page->dom
			->find(".product-items-content article a")
			->map( sub {
				my $url = $_->attr("href");			
				return $url =~ /^http/ ?
					$url :
					$self->getBaseUrl() . $url;
			})
			->each();
	};

	return @urls;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;

	my $title = "";

	eval {
		$title = trim($page->dom->at("h1.product-item-headline")->text);
	};

	return $title;
}

sub getImages
{
	my ($self, $page) = @_;

	my @images = ();

	eval {
		@images = $page->dom
			->find(".product-detail-thumbnails img")
			->attr("src")
			->map(sub {
				return $_ =~ /^http/ ?
					$_ :
					"http:" . $_;
			})
			->each();
	};

	return \@images;
}

sub getColors
{
	my ($self, $page, $pid) = @_;

	my $color;
	eval {
		$color = $page->dom
			->at("label[for=filter-colour-$pid]")
			->attr("title");
	};

	return $color ? [ $color ] : [];
}

sub getSizes
{
	my ($self, $page, $pid) = @_;

	my @sizes;
	eval {
		my %sizesInfo = ();
		$page->dom
			->find("label[for*=filter-size-$pid]")
			->map( sub {
				my ($variant) = $_->attr("for") =~ /(\d+)$/;
				$sizesInfo{$variant} = $_->at("input")->attr("value");
			});
		my (@sizeNums) = keys(%sizesInfo);
		if (scalar(@sizeNums) > 0)
		{
			my $url = $self->getBaseUrl()
				. "en_ca/getAvailability?variants="
				. join(",", @sizeNums);
			my $tmpPage = $self->getRedirectPage($url);
			if ($tmpPage) 
			{
				my $struct;
				eval {
					$struct = decode_json($tmpPage->body);
				};
				if (ref($struct) eq ref([]))
				{
					my $sizeNum;
					foreach $sizeNum (@$struct)
					{
						if (exists($sizesInfo{$sizeNum}))
						{
							push(@sizes, $sizesInfo{$sizeNum});
						}
					}
				}
				else
				{
					@sizes = values(%sizesInfo);
				}
			}
			else
			{
				@sizes = values(%sizesInfo);
			}
		}
	};

	return \@sizes;
}

sub getPrices
{
	my ($self, $page) = @_;

	my $price;
	my $salePrice;

	eval {
		my $priceEl = $page->dom
			->at(".price .product-item-price.product-item-price-discount");
		if ($priceEl)
		{
			$price = filterPrice($priceEl->at(".price-value-original")->text);		
			$salePrice = filterPrice($priceEl->at(".price-value")->text);		
		}
		else
		{
			$price = filterPrice($page->dom
				->at(".price .product-item-price")->at(".price-value")->text
			);
		}
	};

	return ($price, $salePrice);
}

sub getDescription
{
	my ($self, $page) = @_;

	my $description = "";

	eval {
		$description = $page->dom->at(".product-detail-description-text")->text;
	};

	return trim($description);
}

sub getSizingInfo
{
	my ($self, $page) = @_;

	my $url = "";

	eval {
		$url = $page->dom->at(".product-detail-linked-product a")->attr("href");

		$url = $url =~ /^http/ ?
			$url :
			$self->getBaseUrl() . $url;
	};

	return $url;
}

sub getContentAndCare
{
	my ($self, $page) = @_;

	my (@contentInfo) = ();
	my (@careInfo) = ();

	eval {
		@contentInfo = $page->dom
			->find(".article-composition li")
			->map( sub { $_->text; } )
			->each();
	};

	eval {
		@careInfo = $page->dom
			->find(".article-care-instructions li")
			->map( sub { $_->text; } )
			->each();
	};

	return join("\n", @contentInfo, @careInfo);
}

# end products methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl() . "/en_ca/index.html";

	my $page = $self->getRedirectPage($coreUrl, "baseUrl" => $self->getBaseUrl());

	$self->die("Could not load $coreUrl") if (!$coreUrl);

	my @parentCategories = $self->getParentCategories($page);

	my ($category, $subCategory);

	foreach $category (@parentCategories)
	{
		my $categoryUrl = $category->{"level3Url"};
		debug("Handling $categoryUrl ...");
		$page = $self->getRedirectPage($categoryUrl, "baseUrl" => $self->getBaseUrl());

		next if (!$page);

		my @subCategories = (
			{
				"level4" => "",
				"level4Url" => $categoryUrl
			},
			$self->getSubCategories($page)
		);

		foreach $subCategory (@subCategories)
		{
			debug("Handling suburl: " . $subCategory->{"level4Url"} . " ...");
			my @productUrls = $self->getProductsUrls($subCategory->{"level4Url"});

			if (scalar(@productUrls) == 0)
			{
				debug("No products");
				next;
			}

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $category->{"level2"},
				"level3"       => $category->{"level3"},
				"level4"       => $subCategory->{"level4"},
				"productsUrl"  => $subCategory->{"level4Url"},
				"productsCnt"  => scalar(@productUrls),
				"productsUrls" => [ @productUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\.(\d+)\.html$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\.$pid\.html$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
	my $level4 = $info->{"level4"};
 	my $url = $info->{"productsUrls"}->[0];


	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3, $level4);
	my $line = $self->buildCategoriesLine($level2, $level3, $level4);
	my $subLine = $self->buildCategoriesLine($level3, $level4);

	if (lc($level2) eq "new arrivals")
	{
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $line) if ($line);
 		$self->addProductField("mSubCategories", $line) if ($line);
	}
	elsif (lc($level1) eq "sale")
	{
 		$self->addProductField("sSubCategories", $fullLine) if ($fullLine);
 		$self->addProductField("mSubCategories", $fullLine) if ($fullLine);
	}
	elsif (lc($level2) eq "sale")
	{
 		$self->addProductField("sSubCategories", $line) if ($line);
 		$self->addProductField("mSubCategories", $line) if ($line);
	}
	elsif ($level3 =~ /sale/i)
	{
 		$self->addProductField("sSubCategories", $subLine) if ($subLine);
 		$self->addProductField("mSubCategories", $subLine) if ($line);
	}
	else
	{
		$self->setProductField("parentName", $level1);
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
		$self->addProductField("mSubCategories", $line) if ($line && $level1);
	}
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getPage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my ($price, $salePrice) = $self->getPrices($page);
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

	my $pid = $self->getProductField("pid");

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "H & M",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => "H & M",
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $self->getDescription($page),
 		"ContentAndCare"         => $self->getContentAndCare($page),
 		"SizeAndFit"             => undef,
 		"Sku"                    => undef,
 		"ProductId"              => $pid,
 		"Sizes"                  => $self->getSizes($page, $pid),
 		"Colours"                => $self->getColors($page, $pid),
 		"Quantity"               => 0,
 		"SizingInfo"             => $self->getSizingInfo($page),
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
