package Sites::ChildrensPlace::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods
sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("nav.header-global-navigation > ul > li > a")
		->map( sub {
			my $el = $_;
			my $level1 = trim($el->text);
			return undef if (inArray(lc($level1), qw(accessories clearance)));

			return $el->parent->find(".subcategory-primary-menu li a")
				->map( sub {
					return {
						"level1" => $level1,
						"level2" => trim($_->text),
						"level2Url" => $self->joinUrl($_->attr("href"))
					};
				})
				->each();
		})
		->grep( sub { defined($_); } )
		->each();
}

sub getSubcategories
{
	my ($self, $url) = @_;

	my $page = $self->getRedirectBasePageNG($url);
	return () if (!$page);

	my $current;
	my @result = ();
	eval {
		$current = $page->dom
			->at(".leftNavLevel0.selected");

		if (!$current)
		{
			debug("Could not get selected menu");
			return ();
		}

		@result = $current->find("li a")
				->map( sub {
					return {
						"level3" => trim($_->text),
						"level3Url" => $self->joinUrl($_->attr("href"))
					};
				})
				->each();
	};

	return @result;
}

sub getProductUrls
{
	my ($self, $url) = @_;

	my $page = $self->getRedirectBasePageNG($url);
	return () if (!$page);

	my $showAllUrl;
	eval {
		my $url = $page->dom
			->at(".show-all a")->attr("href");
		$showAllUrl = $self->joinUrl($url) if ($url);
	};

	if ($showAllUrl)
	{
		debug("Found show all Url $showAllUrl");
		$page = $self->getRedirectBasePageNG($showAllUrl);
		return () if (!$page);
	}

	my @urls = ();

	eval {
		@urls = $page->dom
			->find(".searchResultSpot .productImage a")
			->map( sub {
				return $self->joinUrl($_->attr("href"));
			})
			->each();
	};

	return @urls;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;

	my $title = "";

	eval {
		$title = $page->dom->at("h1[itemprop=name]")->text;
	};

	return $title;
}

sub getImages
{
	my ($self, $page) = @_;
	my @images = ();

	eval {
		@images = $page->dom
			->find(".product-thumbnails a")
			->map( sub {
				return $self->joinUrl($_->attr("data-target"));
			} )
			->each();
	};

	return \@images;
}

sub getColors
{
	my ($self, $page) = @_;

	my @colors = ();

	eval {
		my $color = $page->dom->at("#colorName")->text;
		push(@colors, $color) if ($color);
	};

	return \@colors;
}

sub getSizes
{
	my ($self, $page) = @_;

	my ($sizesStr) = $page->body =~ /var entitledItem.*?(\[.*?\])/s;
	return [] if (!$sizesStr);
	my $jsonStruct;
	eval {
		$jsonStruct = decode_json($sizesStr);
	};

	return [] if (ref($jsonStruct) ne ref([]));

	my @sizes = ();
	my $item;

	foreach $item (@$jsonStruct)
	{
		if (exists($item->{"qty"})
			&& $item->{"qty"} > 0
			&& exists($item->{"Attributes"})
			&& ref($item->{"Attributes"}) eq ref({}))
		{
			while (my ($k, $v) = each(%{$item->{"Attributes"}}))
			{
				if ($k =~ /Size_/ && $v > 0)
				{
					my ($size) = $k =~ /Size_(.*)/;
					push(@sizes, $size) if ($size);
				}
			}	
		}
	}

	return \@sizes;
}

sub getPrices
{
	my ($self, $page) = @_;

	my ($price, $salePrice);

	eval {
		$price = $page->dom->at(".regular-price")->text;
		$price = filterPrice(trim($price)) if ($price);
	};

	eval {
		$salePrice = $page->dom->at(".sale-price")->text;
		$salePrice = filterPrice(trim($salePrice)) if ($salePrice);
	};

	if (!$price && $salePrice)
	{
		$price = $salePrice;
		$salePrice = undef;
	}

	return ($price, $salePrice);
}

sub getDescription
{
	my ($self, $page) = @_;

	my $description = "";

	eval {
		$description = $page->dom->at(".product-description p")->text;
	};

	return $description;
}

sub getContentAndCare
{
	my ($self, $page) = @_;

	my $description = "";

	eval {
		my @description = $page->dom
			->find(".product-description > ul > li")
			->map( sub { $_->text; } )
			->each();

		$description = join("\n", @description) if (scalar(@description) > 0);
	};

	return $description;
}

# end products methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl() . "shop/ca/home";

	my $page = $self->getRedirectBasePageNG($coreUrl);

	$self->die("Could not load $coreUrl") if (!$coreUrl);

	my @categories = $self->getParentCategories($page);

	my $category;
	foreach $category (@categories)
	{
		my $categoryUrl = $category->{"level2Url"};
		debug("Handling $categoryUrl ...");

		my @subCategories = (
			{
				"level3"    => "",
				"level3Url" => $categoryUrl
			}
		);

		push(@subCategories, $self->getSubcategories($categoryUrl, $category->{"level2"}));

		my $subCategory;
		foreach $subCategory (@subCategories)
		{
			my @productUrls = $self->getProductUrls($subCategory->{"level3Url"});

			if (scalar(@productUrls) == 0)
			{
				debug("No products on " . $subCategory->{"level3Url"});
				next;
			}

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $category->{"level2"},
				"level3"       => $subCategory->{"level3"},
				"productsUrl"  => $subCategory->{"level3Url"},
				"productsCnt"  => scalar(@productUrls),
				"productsUrls" => [ @productUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	if ($url =~ /productId=/)
	{
		my $page = $self->getRedirectBasePageNG($url);
		return undef if (!$page);

		my $pid;
		eval {
			my $pidStr = trim($page->dom->at("#partNumber")->text);
			($pid) = $pidStr =~ /(\d{4,}.*)$/;
			$pid =~ s/_/-/g;
		};

		if ($pid)
		{
			my ($secondPid) = $url =~ /productId=(\d+)/;
			if (!exists($self->{"pidsMap"}))
			{
				$self->{"pidsMap"} = {};
			}
			$self->{"pidsMap"}->{$pid} = $secondPid;
			return $pid;
		}
	}

	my ($pid) = $url =~ /-(\d{4,}.*)$/;
	return $pid if ($pid);
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	if (exists($self->{"pidsMap"}->{$pid}))
	{
		my $secondPid = $self->{"pidsMap"}->{$pid};
		return qr/(productId=$secondPid&|-$pid$)/;
	}
	return qr/-$pid$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	if (!$self->getProductField("parentName"))
	{
		$self->setProductField("parentName", $level1);
	}
	my $parentName = $self->getProductField("parentName");
	return if ($parentName ne $level1);

	my $line = $self->buildCategoriesLine($level2, $level3);
	if ($level2 =~ /new arrival/i)
	{
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $line) if ($line);
	}

	$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	$self->addProductField("mSubCategories", $line) if ($line && $level1);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectBasePageNG($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $pid = $self->getProductField("pid");

	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my ($price, $salePrice) = $self->getPrices($page);
 	my $sale = ($salePrice && $salePrice < $price) ? JSON::true : JSON::false;

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "Childrens Place",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => "Childrens Place",
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $self->getDescription($page),
 		"ContentAndCare"         => $self->getContentAndCare($page),
 		"SizeAndFit"             => undef,
 		"Sku"                    => undef,
 		"ProductId"              => $pid,
 		"Sizes"                  => $self->getSizes($page),
 		"Colours"                => $self->getColors($page),
 		"Quantity"               => 0,
 		"SizingInfo"             => "http://www.childrensplace.com/wcsstore/GlobalSAS/html/size-charts.html",
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
