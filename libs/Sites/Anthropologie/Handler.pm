package Sites::Anthropologie::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(trim inArray removeDuplicates);

use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getCategories
{
	my ($self, $page) = @_;

	# remove sub-sub-categories
	$page->dom
		->find("div.dom-off-menu a.c-main-navigation__a--level-3")
		->remove();

	return $page->dom
		->find("div.dom-off-menu a")
		->map( sub { 
			my $href = $_->attr("href");

			my $url = $href =~ /^http/ ?
				$href :
				$self->getBaseUrl() . $href;

			return {
				"name" => $_->text,
				"url"  => $url
			}
		})
		->each();
}

sub getPagesUrls
{
	my ($self, $url) = @_;

	my $page = $self->getPage($url);

	return () if (!$page);

	my $maxPages;
	eval {
		$maxPages = $page->dom
			->at("li.o-pagination__li.o-pagination__number--next")
			->text;
	};

	return () if (!$maxPages);

	$maxPages = int(trim($maxPages));

	return () if (!$maxPages);

	my @pagesUrls = ($url);
	return @pagesUrls if ($maxPages == 1);

	my $pageNum;
	foreach $pageNum (2 .. $maxPages)
	{
		push(@pagesUrls, $url . "?page=$pageNum");
	}

	return @pagesUrls;
}

sub getProductsUrls
{
	my ($self, $page) = @_;

	my (@productsUrls) = ();

	eval {
		@productsUrls = $page->dom
			->find("a.c-product-tile__image-link")
			->map( sub { 
				my $href = $_->attr("href");

				return $href =~ /^http/ ?
					$href :
					$self->getBaseUrl() . $href;
			})
			->each();
	};

	return @productsUrls;
}

# end links methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $mapUrl = $self->getBaseUrl();

	my $page = $self->getPage($mapUrl);

	my @categories = $self->getCategories($page);
	$self->die("Could not load parent categories") if (!@categories);

	my ($category, $categoryUrl, @pagesUrls, $pageUrl, @productsUrls, $subStruct);

	foreach $category (@categories)
	{
		$categoryUrl = $category->{"url"};

		debug("Handling Category " . $category->{"name"} . " "
			. "$categoryUrl ...");

		@pagesUrls = $self->getPagesUrls($categoryUrl);

		if (scalar(@pagesUrls) == 0)
		{
			debug("Could not load any pages from $categoryUrl");
			next;
		}

		debug("Pages count: " . scalar(@pagesUrls));

		foreach $pageUrl (@pagesUrls)
		{
			$page = $self->getPage($pageUrl);

			if (!$page)
			{
				debug("Could not load any products from $pageUrl");
				next;
			}

			@productsUrls = $self->getProductsUrls($page);

			if (scalar(@productsUrls) == 0)
			{
				debug("Could not load any products from $pageUrl");
				next;
			}

			debug("Products count: " . scalar(@productsUrls));

			 $subStruct = {
				"parentName"   => "",
				"parentUrl"    => "",
				"subName"      => $category->{"name"},
				"subUrl"       => $categoryUrl,
				"pagesUrls"    => [ ],
				"productsUrls" => [ @productsUrls ]
			 };

			 push(@$struct, $subStruct);
		}

	}

	return $struct;
}

sub saveMap
{
	my ($self, $file) = @_;

	my $struct = $self->generateMap();

	$self->saveMapFile($struct, $file);
}

1;
