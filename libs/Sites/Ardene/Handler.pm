package Sites::Ardene::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicates);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

my $productsLimit = 120;

sub getSiteUrl
{
	my ($self) = @_;
	return $self->getBaseUrl() . "en/";
}

# links methods

sub getCategories
{
	my ($self, $page) = @_;

	$page->dom->find("#nav .mobile-only")->remove();

	my @parentUrls = ();

	my @cateogires = $page->dom
		->find("#nav li.grandparent-level")
		->map( sub {
			my $el = $_;

			my $level1 = $el->at("a span") ?
				$el->at("a span")->text :
				$el->at("a")->text;
			my $level1Url = $el->at("a")->attr("href");
			push(@parentUrls, $level1Url) if ($level1Url);

			my $subLevel = $el->find("ul.parent-level");

			if ($subLevel->size == 0)
			{
				return {
					"level1" => $level1,
					"level1Url" => $level1Url,
					"level2" => '',
					"level2Url" => '',
					"level3" => '',
					"level3Url" => $level1Url
				};
			}
			else
			{
				my $level2;
				return $el->find("ul.parent-level > li > a,ul.parent-level > li > ul,ul.extra-links a")
					->map(sub {
						my $el = $_;
						my $tag = $el->type;
						if (lc($tag) eq  "a")
						{
							$level2 = $el->text;
							return {
								"level1" => $level1,
								"level1Url" => $level1Url,
								"level2" => $level2,
								"level2Url" => $el->attr("href"),
								"level3" => "",
								"level3Url" => $el->attr("href")
							};
						}
						else
						{
							return $el->find("a")
								->map(sub {
									return {
										"level1" => $level1,
										"level2" => $level2,
										"level2Url" => "",
										"level3" => $_->text,
										"level3Url" => $_->attr("href")
									};
								})
								->each();
						}
					})
					->each();
			}
		})
		->each();

		my $sub = sub {
			my $subStruct = shift;
			return undef if (! ref($subStruct));
			return undef if (inArray($subStruct->{"level2Url"}, @parentUrls));
			return $subStruct;
		};

	return grep($sub->($_), @cateogires);
}

sub getPagesUrls
{
	my ($self, $url) = @_;

	$url = $url . "?limit=$productsLimit";

	my $page = $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl());
	$url = $self->getLastUrl();

	return () if (!$page);

	my $maxProducts = 0;
	eval {
		my $tmpStr = $maxProducts = $page->dom
			->at(".pager .amount")
			->text;

		($maxProducts) = $tmpStr =~ /of (\d+)$/;
		($maxProducts) = $tmpStr =~ /(\d+) item/i if (!$maxProducts);
		$maxProducts ||= 0;
	};

	$maxProducts = int($maxProducts);
	return () if (!$maxProducts);

	return ($url) if ($maxProducts <= $productsLimit);
	
	my $maxPages = int($maxProducts / $productsLimit) + 1;
	my $pageNum;

	my (@pagesUrls) = ($url);
	foreach $pageNum (2 .. $maxPages)
	{
		push(@pagesUrls, $url . "&p=$pageNum");
	}

	return @pagesUrls;
}

sub getProductsUrls
{
	my ($self, $page) = @_;

	my (@productsUrls) = ();

	eval {
		@productsUrls = $page->dom
			->find("div.category-products a.product-image")
			->map( sub { 
				my $href = $_->attr("href");

				return $href =~ /^http/ ?
					$href :
					$self->getBaseUrl() . $href;
			})
			->each();
	};

	return @productsUrls;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;

	my $title = "";

	eval {
		$title = $page->dom
			->at(".product-main-info .product-name h1")->text;
	};

	return $title;	
}

sub getImages
{
	my ($self, $page) = @_;

	my @images = ();

	eval {
		@images = $page->dom
			->find(".more-views a")
			->attr("rel")
			->map( sub {
				my ($url) = $_ =~ /largeimage: '([^']+)'/;

				return $url;
			})
			->each();
	};

	return \@images;
}

sub getColorsAndSizes
{
	my ($self, $productInfo) = @_;

	if (! ref($productInfo))
	{
		return ([], []);
	}

	my $colorId = 171;
	my $sizesId = 172;

	my $attrs = $productInfo->{"attributes"};
	my @colorsList = ();
	@colorsList = @{$attrs->{$colorId}->{"options"}}
		if (exists($attrs->{$colorId}->{"options"}));

	my @sizesList = ();
	@sizesList = @{$attrs->{$sizesId}->{"options"}}
		if (exists($attrs->{$sizesId}->{"options"}));

	my @colors = map( { $_->{"label"} } @colorsList);
	my @sizes = map( { $_->{"label"} } @sizesList);

	return (\@colors, \@sizes);
}

sub getPrices
{
	my ($self, $page) = @_;

	my $salePrice;
	my $price = filterPrice($page->dom->at(".price-box .price")->text);
	my $percents;

	eval {
		my $bogoPrice = $page->dom->at(".price-box .bogo-price")->text;
		($percents) = $bogoPrice =~ /(\d+)% off/i;
	};

	if ($percents && $percents > 0)
	{
		$salePrice = sprintf("%.2f", $price * (100 - $percents) / 100);
	}

	return ($price, $salePrice);
}

sub getDescription
{
	my ($self, $page) = @_;

	my $description = "";

	eval {
		$description = $page->dom
			->at("meta[name=description]")
			->attr("content");
	};

	return $description;
}

sub getContentAndCare
{
	my ($self, $page) = @_;

	my $description = "";

	eval {
		$description = $page->dom
			->at("p.specifications")->text;
	};

	return $description;
}

sub getProductInfo
{
	my ($self, $page) = @_;

	foreach (split("\n", $page->body))
	{
		if (/var spConfig =/)
		{
			my ($jsonStr) = $_ =~ /.*new Product.Config\((.*)\);/;
			my $jsonStruct;
			eval {
				$jsonStruct = decode_json($jsonStr);
			};
			return $jsonStruct;
		}
	}
}

# end products methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $mapUrl = $self->getBaseUrl();
	debug("Site Url: $mapUrl");

	my $page = $self->getPage($mapUrl);

	my @categories = $self->getCategories($page);
	$self->die("Could not load parent categories") if (!@categories);

	my ($category, $categoryUrl, @pagesUrls, $pageUrl, @productsUrls, $subStruct);

	foreach $category (@categories)
	{
		$categoryUrl = $category->{"level3Url"};

		debug("Handling Category " . $category->{"level1"} . " "
			. $category->{"level2"} . " " . $category->{"level3"}
			. " $categoryUrl ...");

		@pagesUrls = $self->getPagesUrls($categoryUrl);

		if (scalar(@pagesUrls) == 0)
		{
			debug("Could not load any pages from $categoryUrl");
			next;
		}

		debug("Pages count: " . scalar(@pagesUrls));

		@productsUrls = ();
		foreach $pageUrl (@pagesUrls)
		{
			$page = $self->getPage($pageUrl);

			if (!$page)
			{
				debug("Could not load any products from $pageUrl");
				next;
			}

			my @tmpUrls = $self->getProductsUrls($page);
			next if (scalar(@tmpUrls) == 0);
			push(@productsUrls, @tmpUrls);
		}

		if (scalar(@productsUrls) == 0)
		{
			debug("Could not load any products from $pageUrl");
			next;
		}

		debug("Products count: " . scalar(@productsUrls));

		 $subStruct = {
			"level1"   => $category->{"level1"},
			"level2"   => $category->{"level2"},
			"level3"   => $category->{"level3"},
			"level3Url"   => $category->{"level3Url"},
			"productsUrls" => [ @productsUrls ]
		 };

		 push(@$struct, $subStruct);
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\/([^\/]+)\.html$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid\.html/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];


	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3);
	my $line = $self->buildCategoriesLine($level2, $level3);

	if (lc($level1) eq "fresh in")
	{
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $fullLine) if ($fullLine);
	}
	elsif (lc($level1) eq "sale")
	{
 		$self->addProductField("sSubCategories", $fullLine) if ($fullLine);
	}
	else
	{
		$self->setProductField("parentName", $level1);
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
		$self->addProductField("mSubCategories", $line) if ($line);
	}
	$self->addProductField("mSubCategories", $fullLine) if ($fullLine);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl());

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $parentName = $self->getProductField("parentName") || $subStruct->{"level1"};

	my $productInfo = $self->getProductInfo($page);

	my ($colors, $sizes) = $self->getColorsAndSizes($productInfo);

	my ($price, $salePrice) = $self->getPrices($page);
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

 	# my ($description, $contentAndCare, $sizeAndFit) = $self->getDescription($page);

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "ARDENE",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => "ARDENE",
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $self->getDescription($page),
 		"ContentAndCare"         => $self->getContentAndCare($page),
 		"SizeAndFit"             => undef,
 		"Sku"                    => undef,
 		"ProductId"              => $self->getProductField("pid"),
 		"Sizes"                  => $sizes,
 		"Colours"                => $colors,
 		"Quantity"               => 0,
 		"SizingInfo"             => "$url#data",
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => undef,
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
