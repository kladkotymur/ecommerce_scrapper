package Sites::Zara::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim isArray);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);
my $cookies;

# links methods

sub getRealUrl
{
	my ($self, $href) = @_;
	return $href =~ /^\/\// ?
		"http:$href" :
		$self->joinUrl($href);
}

sub getParentCategories
{
	my ($self, $page) = @_;

	# looks ugly, but works
	return $page->dom->find("#menu > ul > li:not(.secondary)")
		->map( sub {
			my $el1 = $_;
			my $level1 = $el1->at("a")->text;

			return $el1->at("ul")->children("li")
				->map( sub {
					my $el2 = $_;
					my $level2 = $el2->at("a")->text;

					my $level2Url = $self->getRealUrl($el2->at("a")->attr("href"));


					my $ul2 = $el2->at("ul");

					if ($ul2)
					{
						return $ul2->children("li")
							->map( sub {
								my $el3 = $_;
								my $level3 = $el3->at("a")->text;
								my $level3Url = $self->getRealUrl($el3->at("a")->attr("href"));
								my $ul3 = $el3->at("ul");

								if ($ul3)
								{
									return $ul3->children("li")
										->map( sub {
											my $el4 = $_;
											my $level4 = $el4->at("a")->text;

											my $level4Url = $self->getRealUrl($el4->at("a")->attr("href"));
											return {
												"level1" => $level1,
												"level2" => $level2,
												"level3" => $level3,
												"level4" => $level4,
												"productsUrl" => $level4Url
											};

										})
										->each();
								}
								else
								{
									return {
										"level1" => $level1,
										"level2" => $level2,
										"level3" => $level3,
										"level4" => "",
										"productsUrl" => $level3Url
									};

								}
							})
							->each();
					}
					else
					{
						return {
							"level1" => $level1,
							"level2" => $level2,
							"level3" => "",
							"level4" => "",
							"productsUrl" => $level2Url
						};
					}

			})
			->each();
		})
		->each();
}

sub getProductUrls
{
	my ($self, $page) = @_;

	my @urls = ();

	eval {
		@urls = $page->dom->find("#products a.item")
			->map( sub {
				my $href = $_->attr("href");
				return $href if ($href =~ /^http/);
				return $href =~ /^\/\// ?
					"http:$href" :
					$self->joinUrl($_->attr);
			})
			->each();
	};

	return @urls;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;
	my $title = "";

	eval {
		$title = $page->dom->at("h1.product-name")->text;
	};

	return $title;
}

sub getImages
{
	my ($self, $page) = @_;
	my @images;	

	eval {
		@images = $page->dom->find("#main-images .image-wrap a")
			->map( sub {
				return $self->getRealUrl($_->attr("href"));
			})
			->each();
	};

	return \@images;
}

sub getDescription
{
	my ($self, $page) = @_;
	my $description = "";

	eval {
		$description = $page->dom->at("#description .description")->text;
	};
	return $description;
}

sub getColors
{
	my ($self, $page) = @_;
	my @colors = ();

	eval {
		my $color = $page->dom->at("._colorName")->text;
		push(@colors, $color) if ($color);
	};

	return \@colors;
}

sub getSizes
{
	my ($self, $page) = @_;
	my @sizes = ();

	eval {
		@sizes = $page->dom->find(".product-size:not(.disabled) .size-name")
			->map( sub { $_->text; } )
			->each();
	};
	return \@sizes;
}

sub getPrices
{
	my ($self, $info) = @_;

	my $salePrice;
	my $price = exists($info->{"price"}) ? $info->{"price"}  : undef;
	my $oldPrice = exists($info->{"oldPrice"}) ? $info->{"oldPrice"}  : undef;

	if ($oldPrice)
	{
		$salePrice = $price;
		$price = $oldPrice;	
	}

	my $formatPrice = sub {
		my $price = shift;
		my (@parts) = $price =~ /(\d+)(\d\d)$/;
		return join(".", @parts);
	};

	$price = $formatPrice->($price) if ($price);
	$salePrice = $formatPrice->($salePrice) if ($salePrice);

	return ($price, $salePrice);
}

sub getContentAndCare
{
	my ($self, $info) = @_;

	my @care;
	my $item;
	if (exists($info->{"detail"}->{"care"}))
	{
		my $care = $info->{"detail"}->{"care"};
		if (isArray($care))
		{
			foreach $item (@$care)
			{
				push(@care, $item->{"description"}) if (exists($item->{"description"}));
			}
		}
	}

	if (exists($info->{"detail"}->{"detailedComposition"}->{"parts"}))
	{
		my $parts = $info->{"detail"}->{"detailedComposition"}->{"parts"};
		if (isArray($parts))
		{
			foreach $item (@$parts)
			{
				next if (!exists($item->{"areas"}));
				my $areas = $item->{"areas"};

				my ($subItem, $description, $component);
				if (isArray($areas))
				{
					foreach $subItem (@$areas)
					{
						next if (!exists($subItem->{"description"}));
						next if (!exists($subItem->{"components"}));
						next if (isArray($subItem->{"components"}));

						$description = $subItem->{"description"};
						push(@care, $description);

						foreach $component (@{$subItem->{"components"}})
						{
							next if (!exists($component->{"percentage"}));
							next if (!exists($component->{"material"}));
							push(@care,
								join(" ", $component->{"percentage"}, $component->{"material"}));
						}
					}
				}

				next if (!exists($item->{"components"}));
				my $components = $item->{"components"};
				if (isArray($components))
				{
						$description = $item->{"description"};
						push(@care, $description) if ($description);

						foreach $component (@{$item->{"components"}})
						{
							next if (!exists($component->{"percentage"}));
							next if (!exists($component->{"material"}));
							push(@care,
								join(" ", $component->{"percentage"}, $component->{"material"}));
						}
				}
			}
		}
	}

	return (scalar(@care) > 0) ? join("\n", @care) : undef;
}

sub getSizingInfo
{
	my ($self, $page) = @_;

	my $info = undef;

	eval {
		$info = $self->getRealUrl($page->dom->at("a.size-guide")->attr("href"));
	};

	return $info;
}

# end products methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl();

	my $page = $self->getRedirectPage($coreUrl . "ca/");
	my $cookies = $self->getAgent()->cookie_jar;

	$self->die("Could not load $coreUrl") if (!$page);

	my @categories = $self->getParentCategories($page);

	my $category;
	foreach $category (@categories)
	{
		my $categoryUrl = $category->{"productsUrl"};
		debug("Handling $categoryUrl ...");

		$page = $self->getRedirectPage($categoryUrl, "cookies" => $cookies);
		my @productUrls = $self->getProductUrls($page);

		if (scalar(@productUrls) == 0)
		{
			debug("Could not extract products");
			next;
		}

		my $subStruct = {
			"level1"       => $category->{"level1"},
			"level2"       => $category->{"level2"},
			"level3"       => $category->{"level3"},
			"level4"       => $category->{"level4"},
			"productsUrl"  => $categoryUrl,
			"productsCnt"  => scalar(@productUrls),
			"productsUrls" => [ @productUrls ]
		};

		push(@$struct, $subStruct);
	}

	return $struct;
}

sub getProductInfo
{
	my ($self, $pid, $cookies) = @_;

	my $url = $self->getBaseUrl() . "ca/en/products-details?productIds[]=$pid&ajax=true";
	my $page = $self->getPage($url, "cookies" => $cookies);

	return undef if (!$page);

	my $info;
	eval {
		$info = decode_json($page->body);
	};

	return $info;
}

sub preSaveProducts
{
	my ($self) = @_;

	my $page = $self->getRedirectPage($self->getBaseUrl() . "ca/");
	$cookies = $self->getAgent()->cookie_jar;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /p(\d+)\.html$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;

	return qr/p$pid\.html$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
	my $level4 = $info->{"level4"};
 	my $url = $info->{"productsUrls"}->[0];

	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3, $level4);	
	my $line2 = $self->buildCategoriesLine($level2, $level3, $level4);	
	my $line3 = $self->buildCategoriesLine($level3, $level4);	
	my $line4 = $self->buildCategoriesLine($level4);	

	my $newIn = "new in";
	my $sale = "sale";

	if (inArray($newIn, map(lc, $level1, $level2, $level3)))
	{
		$self->setProductField("isNewArrival", 1);
	}

	if (lc($level1) eq $newIn)
	{
 		$self->addProductField("naSubCategories", $fullLine) if ($fullLine);
	}
	elsif (lc($level2) eq $newIn)
	{
 		$self->addProductField("naSubCategories", $line2) if ($line2);
	}
	elsif (lc($level3) eq $newIn)
	{
 		$self->addProductField("naSubCategories", $line3) if ($line3);
	}
	elsif (lc($level1) eq $sale)
	{
 		$self->addProductField("sSubCategories", $fullLine) if ($fullLine);
	}
	elsif (lc($level2) eq $sale)
	{
 		$self->addProductField("sSubCategories", $line2) if ($line2);
	}
	elsif (lc($level3) eq $sale)
	{
 		$self->addProductField("sSubCategories", $line3) if ($line3);
	}

	if (!inArray(lc($level1), $newIn, $sale))
	{
		$self->setProductField("parentName", $level1)
			if (!$self->getProductField("parentName"));
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}

	$self->addProductField("mSubCategories", $fullLine) if ($fullLine);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectBasePage($url, "cookies" => $cookies);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $pid = $self->getProductField("pid");
	my $info = $self->getProductInfo($pid, $cookies);

	if (!$info)
	{
		debug("Could not get product info");
		return undef;
	}

	if (isArray($info)
		&& scalar(@$info) > 0)
	{
		$info = $info->[0];
	}

	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my ($price, $salePrice) = $self->getPrices($info);
 	my $sale = ($salePrice && $salePrice < $price) ? JSON::true : JSON::false;

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "Zara",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => "Zara",
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $self->getDescription($page),
 		"ContentAndCare"         => $self->getContentAndCare($info),
 		"SizeAndFit"             => undef,
 		"Sku"                    => undef,
 		"ProductId"              => $pid,
 		"Sizes"                  => $self->getSizes($page),
 		"Colours"                => $self->getColors($page),
 		"Quantity"               => 0,
 		"SizingInfo"             => $self->getSizingInfo($page),
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);
	return \%product;
}

1;
