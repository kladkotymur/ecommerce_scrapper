package Sites::BananaRepublic::Handler;

use strict;
use warnings;

use Sites::GapCa::Handler;
use Common::Utils qw(inArray);
use Data::Dumper;

use base qw(Sites::GapCa::Handler);

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("div#mainNavBROL li.topNavLink a")
		->grep( sub { $_->attr("href") =~ /browse/ } )
		->map( sub { 
			return {
				"level1" => $_->text,
				"level1Url"  => $self->getBaseUrl() . $_->attr("href")
			}
		})
		->grep( sub {
			my $level1 = $_->{"level1"};

			return length($level1) > 0
				&& !inArray(lc($level1),
					("new arrivals", "accessories" ,"sale"));
		})
		->each();
}

sub getStore
{
	return "Banana Republic";
}

sub isNewArrival
{
	my ($self, $level) = @_;

	return (lc($level) eq "what's new");
}

sub isSale
{
	my ($self, $level) = @_;

	return inArray(lc($level), ("special offers", "sale"));
}

sub isFeature
{
	my ($self, $level) = @_;

	return inArray(lc($level), ("featured shops", "collections"));
}

1;
