package Sites::CallitSpring::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim);
use Common::DomUtils qw(getCSS getListCSS);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods
sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("header .main-nav > ul > li > a")
		->map( sub {
			return {
				"level1" => $_->text,
				"level1Url" => $self->joinUrl($_->attr("href"))
			};
		})
		->each();
}

sub getSubcategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find(".navigation-left-rail > ul > li > h4")
		->map( sub {
			my $el = $_;
			my $level2 = $el->at("a")->text;
			my $level2Url = $self->joinUrl($el->at("a")->attr("href"));

			my $subs = $el->parent
				->find(".link-list ul li a");

			if ($subs->size > 0)
			{
				return $subs->map( sub {
					return {
						"level2" => $level2,
						"level2Url" => $level2Url,
						"level3" => $_->text,
						"level3Url" => $self->joinUrl($_->attr("href"))
					};
				})
				->each();
			}
			else
			{
				return {
					"level2" => $level2,
					"level2Url" => $level2Url,
					"level3" => "",
					"level3Url" => $level2Url
				};
			}
		})
		->each();
}

sub getProductUrls
{
	my ($self, $url) = @_;

	my @urls = ();
	my $viewAllUrl = $url . "?show=All&viewAll=false&sort=";

	my $page = $self->getRedirectBasePage($viewAllUrl);
	return @urls if (!$page);

	eval {
		@urls = $page->dom
			->find(".product-tile-container .product-tile a")
			->map(sub {
				return $self->joinUrl($_->attr("href"));
			})
			->each();
	};

	return @urls;
}
# end links methods

# products methods
# end products methods

sub generateMap
{
	my ($self) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl() . "ca/en/";
	my $page = $self->getRedirectBasePage($coreUrl);

	$self->die("Could not load $coreUrl") if (!$page);

	my @categories = $self->getParentCategories($page);

	my $category;
	foreach $category (@categories)
	{
		my $categoryUrl = $category->{"level1Url"};
		debug("Handling $categoryUrl ...");

		$page = $self->getRedirectBasePage($categoryUrl);
		if (!$page)
		{
			debug("Could not load $categoryUrl");
			next;
		}

		my @subCategories = $self->getSubcategories($page);

		my $subCategory;
		foreach $subCategory (@subCategories)
		{
			my @productUrls = $self->getProductUrls($subCategory->{"level3Url"});

			if (scalar(@productUrls) == 0)
			{
				debug("No products on " . $subCategory->{"level3Url"});
				next;
			}

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $subCategory->{"level2"},
				"level3"       => $subCategory->{"level3"},
				"productsUrl"  => $subCategory->{"level3Url"},
				"productsCnt"  => scalar(@productUrls),
				"productsUrls" => [ @productUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\/([\w\-]+)$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	my $fullLine = $self->buildCategoriesLine($level1, $level2, $level3);	
	my $subLine = $self->buildCategoriesLine($level2, $level3);	

	if (lc($level2) eq "just in")
	{
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $subLine);
	}
	elsif (lc($level1) eq "sale")
	{
 		$self->addProductField("sSubCategories", $fullLine);
	}
	else
	{
		$self->setProductField("parentName", $level1)
			if (!$self->getProductField("parentName"));
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}

	if (inArray("online exclusives", map(lc, $level1, $level2, $level3)))
	{
		$self->setProductField("isOnlineExclusive", 1);
	}

	$self->addProductField("mSubCategories", $fullLine);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectBasePage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $parentName = $self->getProductField("parentName");
	$parentName = $subStruct->{"level1"} if (!$parentName);

	my $pid = $self->getProductField("pid");
	my $dom = $page->dom;
	my $title = getCSS($dom, ".product-detail-column.right > h1");
	my @images = map { $_ =~ /^http/ ? $_ : "http:$_" }
		getListCSS($dom, "a.imageLinkThumb .pswp_internal_thumbnail", "data-primaryimagesrc");
	my $description = getCSS($dom, ".ProductDescription .description");
	my $contentAndCare = join("\n", getCSS($dom, ".material"), getCSS($dom, ".sole"));
	chomp($contentAndCare);
	my @colors = getListCSS($dom, "p.image.active img", "alt");
	my @sizes = getListCSS($dom, "p.size");

	my $priceDom = $dom->at(".image.active") ? $dom->at(".image.active")->parent : $dom;
	my $priceSelector = ".price-container a";
	my $realPrice = filterPrice(getCSS($priceDom, "$priceSelector > .price:not(.sale)"));
	my $salePrice = filterPrice(getCSS($priceDom, "$priceSelector > .price.sale"));
	my $nonSalePrice = filterPrice(getCSS($priceDom, "$priceSelector > .strikethrough"));

	my $price = $realPrice ? $realPrice : $nonSalePrice;
	$salePrice = $salePrice ? $salePrice : undef;
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;
	my $isOnlineExclusive = $self->getProductField("isOnlineExclusive") ?
		JSON::true :
		JSON::false;

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "CALL IT SPRING",
 		"Title"                  => $title,
 		"Brand"                  => "CALL IT SPRING",
 		"Images"                 => [ @images ],
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $description,
 		"ContentAndCare"         => $contentAndCare,
 		"SizeAndFit"             => "",
 		"Sku"                    => undef,
 		"ProductId"              => $pid,
 		"Sizes"                  => [ @sizes ],
 		"Colours"                => [ @colors ],
 		"Quantity"               => 0,
 		"SizingInfo"             => "http://www.callitspring.com/ca/en/customerService/sizeGuide",
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => $isOnlineExclusive,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		 "Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
