package Sites::AmericanEagle::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicates);

use Data::Dumper;

use Tie::IxHash;

use base qw(Common::BaseHandler);

# links methods

sub getCategories
{
	my ($self, $page) = @_;

	return grep(defined, $page->dom
		->find("#sitemap .top-link-container")
		->map( sub { 
			my $el = $_;

			my $level1El = $el->at(".tier-1-heading a");
			return undef if (!$level1El);

			my $level1 = $level1El->text;

			return $el->find("div.tier-2")
				->map(sub {
					my $level2Div = $_->at("div.tier-2-heading");
					my $levels3 = $_->find("li");

					my ($level2, $level2Url);
					my $level2DivA = $level2Div->at("a");	

					if ($level2DivA)
					{
						$level2 = $level2DivA->text;
						$level2Url = $self->getBaseUrl() . $level2DivA->attr("href");
					}
					else
					{
						$level2 = $level2Div->text;
						$level2Url = "";
					}

					if ($levels3->size == 0)
					{
						return {
							"level1" => $level1,
							"level2" => $level2,
							"level2Url" => $level2Url,
							"level3" => "",
							"level3Url" => $level2Url
						};
					}

					return $levels3->map(sub {
						my $aTag = $_->at("a");

						return {
							"level1" => $level1,
							"level2" => $level2,
							"level2Url" => $level2Url,
							"level3" => $aTag->text,
							"level3Url" => $self->getBaseUrl() . $aTag->attr("href")
						};

					})
					->each();
				})
				->each();	
		})
		->each()
	);
}

sub getProductsInfo
{
	my ($self, $page) = @_;

	my @info = ();
	my (@tmpUrls) = ();
	my $subLevel;
	eval {
		$page->dom
			->at(".product-list")
			->find("div.section-header,div.product-details-container a")
			->map( sub { 
				my $tag = $_->type;

				if (lc($tag) eq "div")
				{
					if (!$subLevel)
					{
						$subLevel = $_->at(".section-header-title")->text;
					}
					else
					{
						push(@info, {
							"subLevel" => $subLevel,
							"urls" => [ removeDuplicates(@tmpUrls) ]
						});
						$subLevel = $_->at(".section-header-title")->text;
						@tmpUrls = ();
					}
				}
				else
				{
					my $href = $_->attr("href");
					my $url = $href =~ /^http/ ?
						$href :
						$self->getBaseUrl() . $href;

					push(@tmpUrls, $url);
				}
			})
			->each();
	};

	return @info;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;	

	my $title = "";
	eval {
		$title = $page->dom
			->at("h1.psp-product-name")
			->text;
	};

	return $title;
}

sub getColors
{
	my ($self, $page) = @_;

	my $color;

	eval {
		$color = $page->dom
			->at(".psp-product-color span")
			->text;
	};

	return $color ? [ $color ] : [];
}

sub getImages
{
	my ($self, $page) = @_;

	my @images = ();

	eval {
		@images = $page->dom
			->find("#product-carousel .item-img")
			->attr("data-image")
			->map( sub {
				$_ =~ /^http/ ?
					$_ :
					"https:$_";
			})
			->each();	
	};

	return \@images;
}

sub getSizes
{
	my ($self, $page) = @_;

	my @sizes = ();

	eval {
		@sizes = $page->dom
			->find("#psp-sizedropdown-menu li[data-outofstock=false] a")
			->map( sub { $_->text } )
			->each();
	};

	return \@sizes;
}

sub getPrices
{
	my ($self, $page) = @_;

	my $price;
	my $salePrice;

	eval {
		$price = $page->dom->at("#psp-regular-price")->text;
		$salePrice = $page->dom->at("#psp-sale-price")->text;
	};

	return ($price, $salePrice);
}

sub getDescription
{
	my ($self, $page) = @_;
	
	my $description = "";

	eval {
		$description = $page->dom
			->at("meta[property*=description]")
			->attr("content");
	};

	return $description;
}

sub getContentAndCare
{
	my ($self, $page) = @_;

	my $contentAndCare = "";

	eval {
		my @info = $page->dom->find("ul.pdp-about-bullets li")
			->map( sub { $_->text; } )
			->each();

		$contentAndCare = join("\n", @info);
	};

	return $contentAndCare;
}

# end products methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $mapUrl = $self->getBaseUrl() . "/sitemap";

	my $page = $self->getPage($mapUrl);
	my @categories = $self->getCategories($page);
	$self->die("Could not load parent categories") if (!@categories);

	my ($category, $categoryUrl, @productsInfo, $productItem,
		$subStruct, $level3, $level4, $subLevel);

	foreach $category (@categories)
	{
		$categoryUrl = $category->{"level3Url"};

		debug("Handling Category $categoryUrl ...");

		$page = $self->getPage($categoryUrl);

		@productsInfo = $self->getProductsInfo($page);

		foreach $productItem (@productsInfo)
		{
			$subLevel = $productItem->{"subLevel"};
			$level3 = "";
			$level4 = "";

			if (!$category->{"level3"})
			{
				$level3 = $subLevel;
			}
			elsif ($category->{"level3"} eq $subLevel)
			{
				$level3 = $subLevel;
			}
			else
			{
				$level3 = $category->{"level3"};
				$level4 = $subLevel;	
			}

			$subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $category->{"level2"},
				"level3"       => $level3,
				"level4"       => $level4,
				"parentUrl"    => $categoryUrl,
				"productsUrls" => [ @{$productItem->{"urls"}} ]
			};

			 push(@$struct, $subStruct);
		}

	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\/([\d_]+)\?/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid\?/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
	my $level4 = $info->{"level4"};
 	my $url = $info->{"productsUrls"}->[0];

	if (!$self->getProductField("parentName"))
	{
		$self->setProductField("parentName", $level1);
	}
	my $parentName = $self->getProductField("parentName");
	return if ($parentName ne $level1);

	if (inArray("online only", map(lc, $level2, $level3, $level4)))
	{
		$self->setProductField("isOnlineExclusive", 1);
	}

	if (lc($level3) eq "new arrivals")
	{
		my $naLine = $self->buildCategoriesLine($level3, $level4);
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $naLine) if ($naLine);
	}

	my $line = $self->buildCategoriesLine($level2, $level3, $level4);
	if (lc($level2) eq "featured")
	{
 		$self->addProductField("fSubCategories", $line) if ($line);
		$self->addProductField("mSubCategories", $line) if ($line);
		$self->setProductField("isFeature", 1);
	}
	elsif (lc($level2) eq "deals")
	{
 		$self->addProductField("sSubCategories", $line) if ($line);
		$self->addProductField("mSubCategories", $line) if ($line);
	}
	else
	{
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
		$self->addProductField("mSubCategories", $line) if ($line && $level1);
	}
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl());

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my ($price, $salePrice) = $self->getPrices($page);
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

 	my $isOnline = $self->getProductField("isOnlineExclusive") ? JSON::true : undef;

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "American Eagle",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => "American Eagle",
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $self->getDescription($page),
 		"ContentAndCare"         => $self->getContentAndCare($page),
 		"SizeAndFit"             => undef,
 		"Sku"                    => undef,
 		"ProductId"              => $self->getProductField("pid"),
 		"Sizes"                  => $self->getSizes($page),
 		"Colours"                => $self->getColors($page),
 		"Quantity"               => 0,
 		"SizingInfo"             => $url . "#sizechart",
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => $isOnline,
 		"ParentCategory"         => $self->getProductField("parentName"),
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
