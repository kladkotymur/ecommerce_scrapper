package Sites::AdditionElle::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase trim);

use Tie::IxHash;
use Data::Dumper;

use base qw(Common::BaseHandler);

my $pageSize = 12;

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("ul.menu-category.level-1 li[class*=category] a.level-1")
		->map( sub { 
			my $el = $_;
			my $level2 = trim($el->text);
			my $level2Url = $el->attr("href");

			my $subMenu = $el->parent->at("div.menu-wrapper");
			if (!$subMenu)
			{
				return {
					"level1" => "Women",
					"level2" => $level2,
					"level2Url" => $level2Url,
					"level3" => "",
					"level3Url" => $level2Url
				};
			}

			return (
				{
					"level1" => "Women",
					"level2" => $level2,
					"level2Url" => $level2Url,
					"level3" => "",
					"level3Url" => $level2Url
				},
				$subMenu
					->find("a.level-2")
					->map( sub {
						return {
							"level1" => "Women",
							"level2" => $level2,
							"level2Url" => $level2Url,
							"level3" => trim($_->text),
							"level3Url" => $_->attr("href")
						};
					})
					->each()
			);
		})
		->each();
}

sub getSubCategories
{
	my ($self, $page) = @_;

	my @subs = ();
	eval {
		@subs = $page->dom
			->find("ul#category-level-2 li a")
			->map( sub { 
				return {
					"level4" => trim($_->text),
					"level4Url"  => $_->attr("href")
				}
			})
			->each();
	};

	return @subs;
}

sub getPagesUrls
{
	my ($self, $url) = @_;

	my $page = $self->getPage($url);
	return () if (!$page);

	my $count = 0;

	eval {
		$count = $page->dom
			->at("div.results-hits span.count")
			->text();

		$count = int($count);
	};

	debug("Products count: $count");

	return () if ($count == 0);

	my @pagesUrls = ();
	my $sum = 0;

	while ($sum < $count)
	{
		push(@pagesUrls, $url . "?"
			. "sz=$pageSize&"
			. "start=$sum&"
			. "format=page-element");
			
		$sum += $pageSize;
	}

	return @pagesUrls;
}

sub getProductsUrls
{
	my ($self, @pagesUrls) = @_;

	return () if (scalar(@pagesUrls) == 0);

	my @productsUrls = ();

	my ($pageUrl, $page, @tmpUrls);
	foreach $pageUrl (@pagesUrls)
	{
		debug("Handling page $pageUrl ...");

		$page = $self->getPage($pageUrl);
		next if (!$page);

		@tmpUrls = ();
		eval {
			@tmpUrls = $page->dom
				->find("a.name-link")		
				->map( sub {
					my $href= $_->attr("href");

					return $href =~ /^http/ ? $href : $self->getBaseUrl() . $href;	
				})
				->each();
		};

		if (scalar(@tmpUrls) > 0)
		{
			push(@productsUrls, @tmpUrls);
		}
	}

	return @productsUrls;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;

	my $title = "";

	eval {
		$title = $page->dom
			->at(".product-name[itemprop=name]")->text();
	};

	return $title;
}

sub getImages
{
	my ($self, $page) = @_;

	my @images = ();

	eval {
		@images = $page->dom
			->find("a.thumbnail-link")
			->map( sub {
				my $href= $_->attr("href");

				return $href =~ /^http/ ? $href : $self->getBaseUrl() . $href;	
			})
			->each();
	};

	if (scalar(@images) == 0)
	{
		eval {
			my $zoomLink = $page->dom
				->at("a.zoom-link")->attr("href");

			$zoomLink = $zoomLink =~ /^http/ ? $zoomLink : $self->getBaseUrl() . $zoomLink;
			@images = ($zoomLink);
		};
	}

	return @images;
}

sub getColours
{
	my ($self, $page) = @_;

	my @colours = ();

	eval {
		@colours = $page->dom
			->find("ul.swatches.Color li")
			->map( sub {
				return undef if ($_->attr("class") =~ /unselectable/);

				return $_->at("a")->attr("title");
			})
			->each();

		@colours = grep(defined, @colours);
	};

	return @colours;
}

sub getSizes
{
	my ($self, $page) = @_;

	my @sizes = ();

	eval {
		@sizes = $page->dom
			->find("ul.swatches.size li")
			->map( sub {
				return ($_->attr("class") eq "emptyswatch") ?
					$_->at("a")->attr("title") :
					undef;
			})
			->each();
	};

	@sizes = grep( defined, @sizes);

	return @sizes;
}

sub getSizingInfo
{
	my ($self, $page) = @_;

	my $sizingInfo = "";

	eval {
		$sizingInfo = $page->dom
			->at("li.size-chart-link a")
			->attr("href");

		$sizingInfo = $sizingInfo =~ /^http/ ?
			$sizingInfo :
			$self->getBaseUrl() . $sizingInfo;
	};

	return $sizingInfo;
}

sub getProductInfo
{
	my ($self, $page) = @_;

	my %info = (
		"description" => "",
		"contentAndCare" => "",
		"sizeAndFit" => ""
	);

	eval {
		my $tabName = "";

		my @tabs = $page->dom
			->find("ul.tabs-menu li a")
			->map( sub {
				return {
					"name" => $_->text(),
					"href" => $_->attr("href")	
				}
			})
			->each();

		my $tab;
		foreach $tab (@tabs)
		{
			my $id = $tab->{"href"};
			my $name = $tab->{"name"};
			$id =~ s/^#//;

			my $text = "";
			eval {
				my $el = $page->dom->at("#$id");

				if ($name eq "Description")
				{
					$info{"description"} = $el->text();
				}
				elsif ($name eq "Garment Care")
					#|| $name eq "Product Details)
				{
					my @text = $el->find("ul li")->map( sub { $_->text() } )->each();
					$info{"contentAndCare"} = join("\n", @text);
				}
				elsif ($name =~ /Size.+Fit/)
				{
					$info{"sizeAndFit"} = $el->at("p")->text();
				}
			};

			$info{$tab->{"name"}} = $text;
		}
	};

	return %info;
}

sub getPrices
{
	my ($self, $page) = @_;

	my ($regularPrice, $markdownPrice, $promoPrice, $price, $salePrice);

	eval { $regularPrice  = trim($page->dom->at("span.priceRegular")->text()); };
	eval { $markdownPrice = trim($page->dom->at("span.priceMarkdown")->text()); };
	eval { $promoPrice    = trim($page->dom->at("span.pricePromo")->text()); };

	my $tmpPrice = $promoPrice || $markdownPrice;

	($price) = $regularPrice =~ /\$([\d.]+)/ if ($regularPrice);
	($salePrice) = $tmpPrice =~ /\$([\d.]+)/ if ($tmpPrice);

	return ($price, $salePrice);
}

# sub getPageCategories
# {
# 	my ($self, $page) = @_;
# 
# 	my (@categories) = ();
# 
# 	eval {
# 		my @tmp = $page->dom
# 			->find("ol.breadcrumb a")
# 			->map( sub {
# 				my $category = $_->text();
# 
# 				return undef if ($category =~ /Home|New.?Arrival|Sale|Online.?Exclusive/i);
# 				return $category;
# 			})
# 			->each();
# 
# 		@categories = grep(defined, @tmp);
# 	};
# 
# 	return @categories;
# }

# end products methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl();

	my $page = $self->getPage($coreUrl);

	my @parentCategories = $self->getParentCategories($page);
	$self->die("Could not load parent categories") if (!@parentCategories);

	my ($category, $subCategory, @subCategories,
		@pagesUrls, @productsUrls, $subStruct);

	foreach $category (@parentCategories)
	{
		my $categoryUrl = $category->{"level3Url"};
		debug("Handling Parent $categoryUrl ...");

		$page = $self->getPage($categoryUrl);
		next if (!$page);

		@pagesUrls = $self->getPagesUrls($categoryUrl);
		@productsUrls = $self->getProductsUrls(@pagesUrls);

		$subStruct = {
			"level2"       => $category->{"level2"},
			"level2Url"    => $categoryUrl,
			"level3"       => $category->{"level3"},
			"level3Url"    => $category->{"level3Url"},
			"level4"       => "",
			"level4Url"    => "",
			"productsUrls" => [ @productsUrls ]
		};

		push(@$struct, $subStruct);

		@subCategories = $self->getSubCategories($page);

		foreach $subCategory (@subCategories)
		{
			debug("Handling subcategory: " . $subCategory->{"level4Url"});

			@pagesUrls = $self->getPagesUrls($subCategory->{"level4Url"});
			@productsUrls = $self->getProductsUrls(@pagesUrls);

			$subStruct = {
				"level2"       => $category->{"level2"},
				"level2Url"    => $categoryUrl,
				"level3"       => $category->{"level3"},
				"level3Url"    => $category->{"level3Url"},
				"level4"       => $subCategory->{"level4"},
				"level4Url"    => $subCategory->{"level4Url"},
				"productsUrls" => [ @productsUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\/([^\/]+)\.html/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid.html/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
	my $level4 = $info->{"level4"};
 	my $url = $info->{"productsUrls"}->[0];

	if (lc($level2) eq "new arrivals" || lc($level3) eq "new arrivals")
	{
		$self->setProductField("isNewArrival", 1);
	}

	if (lc($level2) eq "features" || lc($level3) eq "features")
	{
		$self->setProductField("isFeature", 1);
	}

	if (lc($level2) eq "online exclusive" || lc($level3) eq "online exclusive")
	{
		$self->setProductField("isOnlineExclusive", 1);
	}

	my $line = $self->buildCategoriesLine($level2, $level3, $level4);
	if (lc($level2) eq "new arrivals")
	{
 		$self->addProductField("naSubCategories", $line) if ($line);
	}
	elsif (lc($level2) eq "features")
	{
 		$self->addProductField("fSubCategories", $line) if ($line);
	}	
	elsif (lc($level2) eq "sale")
	{
 		$self->addProductField("sSubCategories", $line) if ($line);
	}
	else
	{
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}
	$self->addProductField("mSubCategories", $line) if ($line);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getPage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my %product = ();
	tie(%product, "Tie::IxHash");

	my %productInfo = $self->getProductInfo($page);

	my ($price, $salePrice) = $self->getPrices($page);
	my $sale = $salePrice ? JSON::true : JSON::false;

 	my $isOnline = $self->getProductField("isOnlineExclusive") ? JSON::true : undef;

	# my @pageCategories = $self->getPageCategories($page);
	# my (@subCategories) = $self->getProductSubCategories(@pageCategories);

	%product = 
	(
		"Url"                    => $url,
		"Store"                  => "Addition Elle",
		"Title"                  => $self->getTitle($page),
		"Brand"                  => "AdditionElle",
		"Images"                 => [ $self->getImages($page) ],
		"Price"                  => $price,
		"SalePrice"              => $salePrice,
		"Description"            => $productInfo{"description"},
		"ContentAndCare"         => $productInfo{"contentAndCare"},
		"SizeAndFit"             => $productInfo{"sizeAndFit"},
		"Sku"                    =>  undef,
 		"ProductId"              => $self->getProductField("pid"),
		"Sizes"                  => [ $self->getSizes($page) ],
		"Colours"                => [ $self->getColours($page) ],
		"Quantity"               => 0,
		"SizingInfo"             => $self->getSizingInfo($page),
		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => $isOnline,
		"ParentCategory"         => "Women",
		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
		"NewArrival"             => $self->isNewArrivalJSON(),
		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
	);

	return \%product;
}

1;
