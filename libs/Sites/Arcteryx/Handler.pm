package Sites::Arcteryx::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicates);

use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getProductsUrls
{
	my ($self, $page) = @_;

	return $page->dom
		->find("div.searchResult a")
		->map( sub {
			return $_->attr("href") =~ /^http/ ?
				$_->attr("href") :
				$self->getBaseUrl() . $_->attr("href");
		})
		->each();
}

# end links methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $baseUrl = $self->getBaseUrl() . "ProductFind.aspx?country=ua&language=en";

	my ($gender, $page, $url, @productsUrls, $subStruct);

	foreach $gender (qw(mens womens))
	{
		$url = $baseUrl . "&gender=$gender";	

		debug("Handling $url ... ");

		$page = $self->getPage($url);

		@productsUrls = $self->getProductsUrls($page);

		debug("Products count: " . scalar(@productsUrls));

		 $subStruct = {
			"parentName"   => $gender,
			"parentUrl"    => $url,
			"subName"      => "",
			"subUrl"       => "",
			"pagesUrls"    => [ ],
			"productsUrls" => [ @productsUrls ]
		 };

		 push(@$struct, $subStruct);
	}

	return $struct;
}

sub saveMap
{
	my ($self, $file) = @_;

	my $struct = $self->generateMap();

	$self->saveMapFile($struct, $file);
}

1;
