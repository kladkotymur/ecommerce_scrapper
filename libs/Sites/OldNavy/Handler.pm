package Sites::OldNavy::Handler;

use strict;
use warnings;

use Sites::GapCa::Handler;
use Common::Utils qw(inArray);
use Data::Dumper;

use base qw(Sites::GapCa::Handler);

sub getParentCategories
{
	my ($self, $page) = @_;

	$page->dom
		->find("div#divisionContainer ul.submenu")
		->remove();

	return $page->dom
		->find("div#divisionContainer li.division a")
		->grep( sub { $_->attr("href") =~ /browse/ } )
		->map( sub { 
			return {
				"level1" => $_->text,
				"level1Url"  => $self->getBaseUrl() . $_->attr("href")
			}
		})
		->grep( sub { length($_->{"level1"}) > 0 })
		->each();
}

sub getStore
{
	return "Old Navy";
}

sub isNewArrival
{
	my ($self, $level) = @_;

	return (lc($level) eq "new & now");
}

sub isSale
{
	my ($self, $level) = @_;

	return inArray(lc($level), ("deals"));
}

sub isFeature
{
	my ($self, $level) = @_;

	return inArray(lc($level), ("ideas & inspiration", "featured shops"));
}

1;
