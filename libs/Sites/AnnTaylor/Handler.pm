package Sites::AnnTaylor::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicates);

use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("div.main div.section a")
		->map( sub { 
			my $href = $_->attr("href");

			my $url = $href =~ /^http/ ?
				$href :
				$self->getBaseUrl() . $href;

			return {
				"name" => $_->text,
				"url"  => $url
			}
		})
		->each();
}

sub getProducts
{
	my ($self, $url) = @_;

	my $maxPages = 100;

	my @productsUrls = ();
	my ($i, $pageUrl, $page, @tmpUrls);

	foreach $i (1 .. $maxPages)
	{
		$pageUrl = $url . "?goToPage=$i";	
		debug("Handling page url $pageUrl ... ");

		$page = $self->getPage($pageUrl);

		if (!$page)
		{
			debug("Could not load $pageUrl");
			last;
		}

		@tmpUrls = ();

		eval {
			@tmpUrls = $page->dom
				->find("div.product-wrap a")
				->map( sub { 
					my $href = $_->attr("href");

					return $href =~ /^http/ ?
						$href :
						$self->getBaseUrl() . $href;

				})
				->each();
		
		};

		last if (scalar(@tmpUrls) == 0);

		push(@productsUrls, @tmpUrls);
	}

	return @productsUrls;
}

# end links methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $mapUrl = $self->getBaseUrl() . "siteMap.jsp";

	my $page = $self->getPage($mapUrl);

	my @categories = $self->getCategories($page);
	$self->die("Could not load parent categories") if (!@categories);

	my ($category, $categoryUrl, @productsUrls, $subStruct);

	foreach $category (@categories)
	{
		$categoryUrl = $category->{"url"};

		debug("Handling Category " . $category->{"name"} . " "
			. "$categoryUrl ...");


		@productsUrls = $self->getProducts($categoryUrl);

		debug("Count: " . scalar(@productsUrls));

		if (scalar(@productsUrls) == 0)
		{
			debug("Could not load any products from $categoryUrl");
			next;
		}

		 $subStruct = {
		 	"parentName"   => "",
		 	"parentUrl"    => "",
		 	"subName"      => $category->{"name"},
		 	"subUrl"       => $categoryUrl,
		 	"pagesUrls"    => [ ],
		 	"productsUrls" => [ @productsUrls ]
		 };

		 push(@$struct, $subStruct);
	}

	return $struct;
}

sub saveMap
{
	my ($self, $file) = @_;

	my $struct = $self->generateMap();

	$self->saveMapFile($struct, $file);
}

1;
