package Sites::Forever21::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicatesIgnoreCase);

use JSON;
use Tie::IxHash;
use Data::Dumper;

use base qw(Common::BaseHandler);

my $lang = "en-US";

sub getProductUrl
{
	my $self = shift;
	return $self->{"baseUrl"} . "Product/";
}

sub getLevelUrlByJs
{
	my ($self, $url) = @_;

	my ($brand, $category) = $url =~ /fnGoToCategory\('([^']*)','([^']*)'/;
	return $self->getProductUrl() . "Category.aspx?br=$brand&category=$category&lang=$lang";
}

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("div.new_header_left nav li a")
		->grep( sub { $_->attr("href") =~ /^http/ } )
		->map( sub { 
			return {
				"level1" => $_->text,
				"level1Url"  => $_->attr("href") . "&lang=$lang"
			}
		})
		->each();
}

sub getSubCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("div.grandchild_wrapper")
		->map( sub {
			my $node = $_;
			my $level2 = $node->parent->at("a span")->text;	
			my $level2Url = $node->parent->at("a")->attr("href") . "&lang=$lang";

			if ($level2Url =~ /fnGoToCategory/)
			{
				$level2Url = $self->getLevelUrlByJs($level2Url);
			}

			return (
					{
						"level2" => $level2,
						"level2Url" => $level2Url,
						"level3" => "",
						"level3Url" => $level2Url
					},
					$node->find("nav li a")
						->grep( sub { $_->attr("href") =~ /^http/ } )
						->map( sub {
							return (
								{
									"level2" => $level2,
									"level2Url" => $level2Url,
									"level3" => $_->text,
									"level3Url" => $_->attr("href") . "&lang=$lang"
								}
							);
						})
						->each()
			);
		})
		->each();
}

sub getCategoryPages
{
	my ($self, $page) = @_;

	my @realPageLinks = ();
	eval {
		my @pageLinks = $page->dom
			->find("div.c_pagination_list button")
			->attr("onclick")
			->grep( qr/location\.href/ )
			->uniq()
			->map( sub {
				s/location\.href=.([^']+)'.*/$1/;
				$_
			})
			->each();

		return () if (! @pageLinks);

		@pageLinks = sort {
			my ($da) = $a =~ /page=(\d+)/;
			my ($db) = $b =~ /page=(\d+)/;
			$da <=> $db;
		} @pageLinks;

		my $lastPage = pop(@pageLinks);

		my ($subUrl, $pageSize, $maxNum) = $lastPage =~ /(.+)pagesize=(\d+)&page=(\d+)/;
		
		return () if ($maxNum < 2);

		my $num;
		for $num (2 .. $maxNum)
		{
			push(@realPageLinks, $self->getProductUrl() . "Category.aspx"
				. $subUrl . "pagesize=$pageSize&page=$num");
		}

		return @realPageLinks;
	};

	return @realPageLinks;
}

sub getProductsUrls
{
	my ($self, $page) = @_;

	return $page->dom
		->find("a")
		->attr("href")
		->grep( qr/ProductID=\d+/ )
		->each();
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;

	my $title = "";
	eval {
		$title = $page->dom
			->at("div.pdp_title h1.item_name_p")
			->text();
	};

	return $title;
}

sub getBrand
{
	my ($self, $page) = @_;

	my $title = "";
	eval {
		$title = $page->dom
			->at("div.pdp_title h1.brand_name_p")
			->text();
	};

	return $title;
}

sub getImages
{
	my ($self, $page) = @_;

	my $images = [];

	eval {
		my @images = $page->dom
			->find('a[href*="fnChangeImage"]')
			->attr("href")
			->map( sub {
				s/.*(http[^']+).*/$1/;
				$_;
			})
			->each();

		$images = [ @images ];
	};
	return $images;
}

sub getSizes
{
	my ($self, $page) = @_;

	my $sizes = [];

	eval {
		my @sizes = $page->dom
			->find('li[onclick*=fnChangeSize] label')
			->map( sub { $_->text() } )
			->each();

		$sizes = [ @sizes ];
	};
	return $sizes;
}

sub getColours
{
	my ($self, $page) = @_;

	my $colours = [];

	eval {
		my @colours = $page->dom
			->find('a[href*=fnChangeColor]')
			->attr('href')
			->map( sub {
				my @args = $_ =~ /'([^']+)'/g;
				return (scalar(@args) == 4) ? $args[3] : "";
			})
			->grep( qr/.+/ )
			->each();

		$colours = [ @colours ];
	};
	return $colours;
}

sub getPrice
{
	my ($self, $page) = @_;

	my $price = "0.0";

	eval {
		$price = $page->dom
			->at("span.price_c.original")
			->text();
	};

	if ($@)
	{
		eval {
			$price = $page->dom
				->at("span[itemprop=price]")
				->text();
		};
	}

	$price =~ s/[^\d.]//g;
	return $price;
}

sub getSalePrice
{
	my ($self, $page) = @_;

	my $price = undef;

	eval {
		$price = $page->dom
			->at("span.price_c.sale")
			->text();
	};

	$price =~ s/[^\d.]//g if ($price);
	return $price;
}

# sub getPageSubcategories
# {
# 	my ($self, $page) = @_;
# 
# 	my @subCategories =  ();
# 
# 	eval {
# 		@subCategories = $page->dom
# 			->find("div#div_breadcrumb a")
# 			->map( sub { $_->text() } )
# 			->grep( sub { 
# 				$_ !~ /^home$/i;
# 			})
# 			->each();
# 
# 		shift(@subCategories) if (scalar(@subCategories) > 0); # skip parent
# 	};
# 	return @subCategories;
# }

sub getSizingInfo
{
	my ($self, $page) = @_;

	my $brand ='';
	my $category = '';

	eval {
		my $href = $page->dom
			->at("a[href*=fnShowSizechart]")
			->attr("href");

		($brand, $category) = $href =~ /'(.*)','(.*)'/;
	};

	my $subUrl;

	if (lc($brand) eq "love21")
	{
		$subUrl = "htmls/sizechart/m_sizecontemp.html?1";
	}
	elsif (lc($brand) eq "21men" || $category =~ /mens/i)
	{
		$subUrl = "htmls/sizechart/m_sizemen.html?1";
	}
	elsif (lc($brand) eq "plus")
	{
		$subUrl = "htmls/sizechart/m_sizeplus.html?1";
	}
	elsif (inArray(lc($brand), qw(girls boys kids)))
	{
		$subUrl = "htmls/sizechart/m_sizekids.html?1";
	}
	elsif ($category =~ /acc|shoes/i)
	{
		$subUrl = "htmls/sizechart/m_sizeacc.html?1";
	}
	else
	{
		$subUrl = "htmls/sizechart/m_sizewomen.html?1";
	}

	return $self->getBaseUrl() . $subUrl;
}

sub getDescription
{
	my ($self, $page) = @_;

	my ($description, $contentAndCare, $sizeAndFit) = ("", "", "");

	eval {
		$page->dom
			->find("div.description_wrapper style")
			->remove();

		my @nodes = $page->dom
			->find("div.description_wrapper section")
			->map( sub {
				my @contentList = ();
				my $content = "";

				eval {
					@contentList = $_->find("div.d_content p")->map("text")->each();
				};

				if (scalar(@contentList) > 0)
				{
					$content = join("\n", @contentList);
				}
				else
				{
					$content = join("\n", $_->find("div.d_content")->all_text()->each());
				}

				return {
					"name" => $_->at("h3")->text(),
					"content" => $content
				};

			})
			->each();

		my $node;
		foreach $node (@nodes)
		{
			$description = $node->{"content"}    if ($node->{"name"} =~ /details/i);
			$contentAndCare = $node->{"content"} if ($node->{"name"} =~ /Content \+ Care/i);
			$sizeAndFit = $node->{"content"}     if ($node->{"name"} =~ /Size \+ Fit/i);
		}
	};

	if ($@)
	{
		eval {
			$description = $page->dom
				->at("div.description_wrapper")
				->all_text();

			$description =~ s/Model Info.*$//;
		};
	}

	return ($description, $contentAndCare, $sizeAndFit);
}

# end products methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $coreUrl = $self->getProductUrl() . "main.aspx?br=f21";

	my $page = $self->getRedirectPage($coreUrl);

	$self->die("Could not load $coreUrl") if (!$coreUrl);

	my @parentCategories = $self->getParentCategories($page);
	$self->die("Could not load parent categories") if (!@parentCategories);

	my ($category, $subCategory, @subCategories);

	foreach $category (@parentCategories)
	{
		my $categoryUrl = $category->{"level1Url"};
		debug("Handling Parent $categoryUrl ...");

		$page = $self->getRedirectPage($categoryUrl);

		next if (!$page);

		@subCategories = $self->getSubCategories($page);

		foreach $subCategory (@subCategories)
		{
			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $subCategory->{"level2"},
				"level3"       => $subCategory->{"level3"},
				"level1Url"    => $categoryUrl,
				"level2Url"    => $subCategory->{"level2Url"},
				"level3Url"    => $subCategory->{"level3Url"},
				"productsUrls" => []
			};

			my $subCategoryUrl = $subCategory->{"level3Url"};

			debug("Handling Sub $subCategoryUrl");

			$page = $self->getRedirectPage($subCategoryUrl);	
			next if (!$page);

			my @subCategoryPages = ($subCategory->{"level3Url"});
			push(@subCategoryPages, $self->getCategoryPages($page));

			my ($pageUrl, @productsUrls);
			foreach $pageUrl (@subCategoryPages)
			{
				debug("Handling Page $pageUrl ... ");

				$page = $self->getRedirectPage($pageUrl);	
				next if (!$page);

				@productsUrls = $self->getProductsUrls($page);

				push(@{$subStruct->{"productsUrls"}}, @productsUrls);
			}

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /ProductID=(\d+)/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/&ProductID=$pid&/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	if (lc($level2) eq "new arrivals" || lc($level3) eq "new arrivals")
	{
		$self->setProductField("isNewArrival", 1);
	}

	if (lc($level2) eq "features" || lc($level3) eq "features")
	{
		$self->setProductField("isFeature", 1);
	}

	my $line = $self->buildCategoriesLine($level2, $level3);
	if (lc($level2) eq "new arrivals")
	{
 		$self->addProductField("naSubCategories", $line) if ($line);
	}
	elsif (lc($level2) eq "features")
	{
 		$self->addProductField("fSubCategories", $line) if ($line);
	}	
	elsif (lc($level2) eq "sale")
	{
 		$self->addProductField("sSubCategories", $line) if ($line);
	}
	else
	{
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}
	$self->addProductField("mSubCategories", $line) if ($line);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getPage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my $parentName = $subStruct->{"level1"};

 	my $salePrice = $self->getSalePrice($page);
	my $price = $self->getPrice($page);

 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

 	my ($description, $contentAndCare, $sizeAndFit) = $self->getDescription($page);

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "Forever",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => $self->getBrand($page),
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $description,
 		"ContentAndCare"         => $contentAndCare,
 		"SizeAndFit"             => $sizeAndFit,
 		"Sku"                    => undef,
 		"ProductId"              => $self->getProductField("pid"),
 		"Sizes"                  => $self->getSizes($page),
 		"Colours"                => $self->getColours($page),
 		"Quantity"               => 0,
 		"SizingInfo"             => $self->getSizingInfo($page),
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $parentName,
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
