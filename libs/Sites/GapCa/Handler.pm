package Sites::GapCa::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray trim removeDuplicates removeDuplicatesIgnoreCase);

use JSON;
use Tie::IxHash;
use Data::Dumper;

use base qw(Common::BaseHandler);

my $lang = "en_CA";
my $cookies;

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	return $page->dom
		->find("div#mainNavGOL li.topNavItem a")
		->grep( sub { $_->attr("href") =~ /browse/ } )
		->map( sub { 
			return {
				"level1" => $_->text,
				"level1Url"  => $self->getBaseUrl() . $_->attr("href")
			}
		})
		->grep( sub { length($_->{"level1"}) > 0 })
		->each();
}

sub getSubCategories
{
	my ($self, $page) = @_;

	my $level2 = '';

	my (@subCategories) = $page->dom
		->at("nav.sidebar-navigation")
		->find("h2,a.sidebar-navigation--category--link")
		#->find("nav.sidebar-navigation h2")
		->map( sub {
			my $tag = $_->type;

			if (lc($tag) eq "h2")
			{
				$level2 = $_->at("span")->text;
				return undef;
			}

			return {
				"level2" => $level2,
				"level3" => $_->at("span")->text,
				"level3Url" => $self->getBaseUrl() . $_->attr("href")
			};
		})
		->each();

	@subCategories = grep( defined, @subCategories );
	return @subCategories;
}

sub getPageProductLinks
{
	my ($self, $url) = @_;
	debug("Handle Page Url: $url");

	my @productLinks = ();

	my $page = $self->getPage($url, "cookies" => $cookies);
	return @productLinks if (!$page);

	my $body = $page->body;

	my $productUrl = $self->getBaseUrl() . "browse/product.do?";

	my ($struct, $id, $pid);
	eval {
		$struct = decode_json($body);
		my $productCategory = $struct->{"productCategoryFacetedSearch"}
			->{"productCategory"};

		$id = $productCategory->{"businessCatalogItemId"};

		if (exists($productCategory->{"childProducts"}))
		{
			if (ref($productCategory->{"childProducts"}) eq ref({}))
			{
				$productCategory->{"childProducts"} = [ $productCategory->{"childProducts"} ];
			}

			foreach (@{$productCategory->{"childProducts"}})
			{
				$pid = $_->{"businessCatalogItemId"};
				push(@productLinks, $productUrl . "cid=$id&pid=$pid");
			}
		}

		if (exists($productCategory->{"childCategories"}))
		{
			my $tmpStruct;
			if (ref($productCategory->{"childCategories"}) eq ref({}))
			{
				$productCategory->{"childCategories"} = [ $productCategory->{"childCategories"} ];
			}

			foreach $tmpStruct (@{$productCategory->{"childCategories"}})
			{
				$id = $tmpStruct->{"businessCatalogItemId"};

				if (exists($tmpStruct->{"childProducts"}))
				{
					if (ref($tmpStruct->{"childProducts"}) eq ref({}))
					{
						$tmpStruct->{"childProducts"} = [ $tmpStruct->{"childProducts"} ];
					}

					foreach (@{$tmpStruct->{"childProducts"}})
					{
						$pid = $_->{"businessCatalogItemId"};
						push(@productLinks, $productUrl . "cid=$id&pid=$pid");
					}
				}	
			}
		}
	};

	if ($@)
	{
		logit("Could not load Products from $url : $@");
		return @productLinks;
	}

	return @productLinks;
}

sub getProductLinks
{
	my ($self, $url) = @_;

	my ($cid) = $url =~ /cid=(\d+)/;

	my $categoryUrl = $self->getBaseUrl() . "/resources/productSearch/v1/search?"
		. "isFacetsEnabled=true&"
		. "locale=$lang&";

	my $firstUrl = $categoryUrl . "cid=$cid&pageId=0";
	my $page = $self->getPage($firstUrl, "cookies" => $cookies);
	return [] if (!$page);

	my $body = $page->body;

	my (@links) = ($firstUrl);
	
	my $struct;
	eval {
		$struct = decode_json($body);
		my $productCategory = $struct->{"productCategoryFacetedSearch"}
			->{"productCategory"};

		my $paginator = $productCategory->{"productCategoryPaginator"};
		my $maxPages = int($paginator->{"pageNumberTotal"});

		if ($maxPages > 1)
		{
			my $i;
			for $i (1 .. $maxPages)
			{
				last if ($i == $maxPages);
				push(@links, $categoryUrl . "cid=$cid&pageId=$i");
			}
		}
	};

	if ($@)
	{
		debug("Could not load page count from $firstUrl $@");
		return [];
	}

	my @productLinks = ();
	my $pageUrl;

	foreach $pageUrl (@links)
	{
		my @tmpLinks = $self->getPageProductLinks($pageUrl);
		if (scalar(@tmpLinks) > 0)
		{
			push(@productLinks, @tmpLinks);
		}
	}

	return @productLinks;
}

# end links methods

# products methods

sub getImages
{
	my ($self, $productInfo, $colorItem, $pid) = @_;

	my $productImages = $productInfo->{"productImages"};
	my @keys = grep( /^${pid}_/, @{$colorItem->{"productStyleColorImages"}});
	my @images = ();

	if (exists($colorItem->{"largeImagePath"}))
	{
		push(@images,  $self->getBaseUrl() . $colorItem->{"largeImagePath"});
	}

	my ($key, $item);
	foreach $key (@keys)
	{
		if (exists($productImages->{$key}))
		{
			$item = $productImages->{$key};
			push(@images, $self->getBaseUrl() . $item->{"large"}) if ($item->{"large"});
		}
	}

	@images = removeDuplicatesIgnoreCase(@images);

	return [ @images ];
}

sub getPrice
{
	my ($self, $colorItem) = @_;

	my ($price) = $colorItem->{"localizedRegularPrice"} =~ /\$([\d.]+)/;

	return $price;
}

sub getSalePrice
{
	my ($self, $colorItem) = @_;

	my $price;
	if (exists($colorItem->{"partialMupMessage"})
		&& $colorItem->{"partialMupMessage"})
	{
		($price) = $colorItem->{"partialMupMessage"} =~ /\$([\d.]+)/;
	}
	else
	{
		($price) = $colorItem->{"localizedCurrentPrice"} =~ /\$([\d.]+)/;
	}

	return $price;
}

sub getDescription
{
	my ($self, $productInfo) = @_;

	if (exists($productInfo->{"infoTabs"}->{"overview"}))
	{
		return join("\n",
			@{$productInfo->{"infoTabs"}->{"overview"}->{"bulletAttributes"}});
	}

	return "";
}

sub getContentAndCare
{
	my ($self, $productInfo) = @_;

	if (exists($productInfo->{"infoTabs"}->{"fabric"}))
	{
		return join("\n",
			@{$productInfo->{"infoTabs"}->{"fabric"}->{"bulletAttributes"}});
	}

	return "";
}

sub getSizeAndFit
{
	my ($self, $productInfo) = @_;

	if (exists($productInfo->{"fitInformation"}->{"bulletAttributes"}))
	{
		return join("\n",
			grep( !/^</,
				@{$productInfo->{"fitInformation"}->{"bulletAttributes"}}));
	}

	return "";
}

sub getVariant
{
	my ($self, $productInfo) = @_;

	my @variants = @{$productInfo->{"variants"}};
	my $variant;
	
	# check regular
	foreach $variant (@variants)
	{
		return $variant if ($variant->{"name"} eq "Regular"
			&& exists($variant->{"productStyleColors"})
			&& $variant->{"inStock"} ne JSON::false);
	}

	# check any in stock
	foreach $variant (@variants)
	{
		return $variant if ($variant->{"inStock"} ne JSON::false
			&& exists($variant->{"productStyleColors"}));
	}

	# let it be first
	return $variants[0];
}

sub getColorItem
{
	my ($self, $productInfo, $pid) = @_;

	my $variant = $self->getVariant($productInfo);
	my $colorsSuperList = $variant->{"productStyleColors"};
	my $colorsSubList;
	my $item;
	
	foreach $colorsSubList (@$colorsSuperList)
	{
		foreach $item (@$colorsSubList)
		{
			return $item if ($item->{"businessCatalogItemId"} eq $pid
				&& $item->{"inStock"} ne JSON::false);
		}
	}
}

sub getColoursAndSizes
{
	my ($self, $colorItem) = @_;

	my $colours = [ $colorItem->{"colorName"} ];
	my @sizes = ();
	my $sizeItem;

	foreach $sizeItem (@{$colorItem->{"sizes"}})
	{
		if ($sizeItem->{"inStock"} ne JSON::false)
		{
			push(@sizes, $sizeItem->{"sizeDimension1"});
		}
	}
	@sizes = map { trim($_) } grep(defined, @sizes);

	return ($colours, \@sizes);	
}

sub getSizingInfo
{
	my ($self, $page) = @_;

	my $sizingInfo = "";

	eval {
		$sizingInfo = $page->dom
			->at("a.size-guide")
			->attr("data-url");

		$sizingInfo = $self->getBaseUrl() . $sizingInfo;
	};

	return $sizingInfo;
}

sub isOnline
{
	my ($self, $productInfo) = @_;

	return undef if (! exists($productInfo->{"marketingFlag"}));
	return undef if (! $productInfo->{"marketingFlag"});
	return $productInfo->{"marketingFlag"} =~ /online.exclusive/i;
}

sub getProductInfo
{
	my ($self, $page) = @_;

	foreach (split("\n", $page->body))
	{
		if (/gap.pageProductData =/)
		{
			my ($jsonStr) = $_ =~ /.*gap.pageProductData = (.*);$/;
			my $jsonStruct;
			eval {
				$jsonStruct = decode_json($jsonStr);
			};

			return $jsonStruct;
		}
	}

	return undef;
}

# end products methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl();

	my $page = $self->getRedirectPage($coreUrl);
	$cookies = $self->getAgent()->cookie_jar;

	$page = $self->getRedirectPage($coreUrl, "cookies" => $cookies);

	my @parentCategories = $self->getParentCategories($page);
	$self->die("Could not load parent categories") if (!@parentCategories);

	my ($category, $subCategory, @subCategories);

	foreach $category (@parentCategories)
	{
		my $categoryUrl = $category->{"level1Url"};
		debug("Handling Parent $categoryUrl ...");

		$page = $self->getPage($categoryUrl, "cookies" => $cookies);
		next if (!$page);

		@subCategories = $self->getSubCategories($page);
		foreach $subCategory (@subCategories)
		{
			debug("Handling subcategory: " . $subCategory->{"level3Url"});
			my (@links) = $self->getProductLinks($subCategory->{"level3Url"});

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $subCategory->{"level2"},
				"level3"       => $subCategory->{"level3"},
				"parentUrl"    => $categoryUrl,
				"subUrl"       => $subCategory->{"level3Url"},
				"productsUrls" => [ @links ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub preSaveProducts
{
	my ($self) = @_;

	my $page = $self->getRedirectPage($self->getBaseUrl());
	$cookies = $self->getAgent()->cookie_jar;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /&pid=(\d+)/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/pid=$pid$/;
}

sub resetProductFields
{
	my $self = shift;

	$self->SUPER::resetProductFields();
	$self->setProductField("categories", []);
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	my $line = $self->buildCategoriesLine($level2, $level3);

	if ($self->isNewArrival($level2))
	{
		$self->setProductField("isNewArrival", 1);
 		$self->addProductField("naSubCategories", $line) if ($line);
	}
	elsif ($self->isFeature($level2))
	{
		$self->setProductField("isFeature", 1);
 		$self->addProductField("fSubCategories", $line) if ($line);
	}	
	elsif ($self->isSale($level2))
	{
 		$self->addProductField("sSubCategories", $line) if ($line);
	}
	else
	{
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}
	$self->addProductField("mSubCategories", $line) if ($line);
}

sub isNewArrival
{
	my ($self, $level) = @_;

	return (lc($level) eq "new & now");
}

sub isSale
{
	my ($self, $level) = @_;

	return inArray(lc($level), ("winter sale", "deal", "deals"));
}

sub isFeature
{
	my ($self, $level) = @_;

	return inArray(lc($level), ("features", "our favourites"));
}

sub getStore
{
	return "Gap";
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getPage($url, "cookies" => $cookies);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	# my $parentName = $self->getProductField("parentName") || $subStruct->{"level1"};
	my $parentName = $subStruct->{"level1"};

	my $productInfo = $self->getProductInfo($page);

	if (!$productInfo)
	{
		debug("Error could not load product info");
		return undef;
	}

	my $colorItem = $self->getColorItem($productInfo, $self->getProductField("pid"));
	if (!$colorItem)
	{
		debug("Error could not load color info");
		return undef;
	}

	my $price = $self->getPrice($colorItem);
	my $salePrice = $self->getSalePrice($colorItem);

	if ($salePrice >= $price)
	{
		$salePrice = undef;
	}

	my $sale = ($salePrice && $salePrice > 0) ? JSON::true : JSON::false;
 	my $isOnline = $self->isOnline($productInfo) ? JSON::true : undef;

	my ($colours, $sizes) = $self->getColoursAndSizes($colorItem);

	my %product = ();
	tie(%product, "Tie::IxHash");

	my $pid = $self->getProductField("pid");
	%product = 
	(
		"Url"                    => $url,
		"Store"                  => $self->getStore(),
		"Title"                  => $productInfo->{"name"},
		"Brand"                  => $self->getStore(),
		"Images"                 => $self->getImages($productInfo, $colorItem, $pid),
		"Price"                  => $price,
		"SalePrice"              => $salePrice,
		"Description"            => $self->getDescription($productInfo),
		"ContentAndCare"         => $self->getContentAndCare($productInfo),
		"SizeAndFit"             => $self->getSizeAndFit($productInfo),
		"Sku"                    =>  undef,
 		"ProductId"              => $pid,
		"Sizes"                  => $sizes,
		"Colours"                => $colours,
		"Quantity"               => 0,
		"SizingInfo"             => $self->getSizingInfo($page),
		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => $isOnline,
		"ParentCategory"         => $parentName,
		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
		"NewArrival"             => $self->isNewArrivalJSON(),
		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
	);

	return \%product;
}

1;
