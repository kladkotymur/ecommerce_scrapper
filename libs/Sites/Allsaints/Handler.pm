package Sites::Allsaints::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicates);

use URI::Encode qw(uri_encode);
use JSON;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getCategories
{
	my ($self, $page) = @_;

	my $type;

	my (@categories, @tmpCategories) = ();
	foreach $type (qw(women men))
	{
		@tmpCategories = $page->dom
			->find("li[data-div=$type] li a")
			->map( sub { 
				return {
					"parentName" => $type,
					"name" => $_->attr("data-label"),
					"path" => $_->attr("href"),
					"url"  => $self->getBaseUrl() . $_->attr("href")
				}
			})
			->each();

		push(@categories, @tmpCategories);
	}

	return @categories;
}

sub getProducts
{
	my ($self, $page) = @_;

	my ($struct, @productsUrls);
	eval {
		$struct = decode_json($page->body);
		@productsUrls = map( { $_->{"url"} } @{$struct->{"products"}});
	};

	return @productsUrls;
}

# end links methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl() . "women/";

	my $page = $self->getRedirectPage($coreUrl);

	my @categories = $self->getCategories($page);
	$self->die("Could not load parent categories") if (!@categories);

	my $apiUrl = $self->getBaseUrl() . "api/category/";

	my ($category, $categoryUrl, @productsUrls, $subStruct);

	foreach $category (@categories)
	{
		$categoryUrl = $apiUrl . "?"
			. "uri=" . uri_encode($category->{"path"})
			. "&view=all";

		debug("Handling Category $categoryUrl ...");

		$page = $self->getRedirectPage($categoryUrl);

		@productsUrls = $self->getProducts($page);

		if (scalar(@productsUrls) == 0)
		{
			debug("Could not load any products from $categoryUrl");
			next;
		}

		$subStruct = {
			"parentName"   => $category->{"parentName"},
			"parentUrl"    => "",
			"subName"      => $category->{"name"},
			"subUrl"       => $category->{"url"},
			"pagesUrls"    => [ ],
			"productsUrls" => [ @productsUrls ]
		};

		push(@$struct, $subStruct);
	}

	return $struct;
}

sub saveMap
{
	my ($self, $file) = @_;

	my $struct = $self->generateMap();

	$self->saveMapFile($struct, $file);
}

1;
