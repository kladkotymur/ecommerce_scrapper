package Sites::AldoShoes::Handler;

use strict;
use warnings;

use Common::BaseHandler;
use Common::Logger qw(debug logit);
use Common::Utils qw(inArray removeDuplicates);
use Common::Filters qw(filterPrice);

use Tie::IxHash;
use Data::Dumper;

use base qw(Common::BaseHandler);

# links methods

sub getParentCategories
{
	my ($self, $page) = @_;

	$page->dom
		->find("div.sub-nav")
		->remove();

	return $page->dom
		->find("nav.navigation li a")
		->map( sub { 
			return {
				"level1" => $_->text,
				"level1Url"  => $self->getBaseUrl() . $_->attr("href")
			}
		})
		->each();
}

sub getSubCategories
{
	my ($self, $page, $level1) = @_;

	my $level2 = '';

	my %clearanceInfo = ();

	return $page->dom
		->find("div.navigation-left-rail > ul > li")
		->map( sub { 
			my $el = $_;

			my $level2 = $el->at("h4 a")->text;
			my $level2Url = $self->getBaseUrl() . $el->at("h4 a")->attr("href"),

			my ($levels3) = $el->find(".link-list li");

			if (lc($level1) eq "outlet"
				&& lc($level2) eq "clearance")
			{
				my @clearanceUrls = $self->getClearanceUrls($level2Url);

				my $sub = sub {
					return {
						"level2" => $level2,
						"level2Url" => $level2Url,
						"level3" => $_[0]->{"level3"},
						"level3Url" => $_[0]->{"level3Url"}
					};
				};

				if (scalar(@clearanceUrls) > 0)
				{
					return map({ $sub->($_) } @clearanceUrls);
				}
			}

			if ($levels3->size == 0)
			{
				return {
					"level2" => $level2,
					"level2Url" => $level2Url,
					"level3" => "",
					"level3Url" =>  $level2Url
				};
			}
			
			return $levels3->map(sub {
				my $aTag = $_->at("a");
				return {
					"level2" => $level2,
					"level2Url" => $level2Url,
					"level3" => $aTag->text,
					"level3Url" =>  $self->getBaseUrl() . $aTag->attr("href")
				};
			})
			->each();
		})
		->each();
}

sub getClearanceUrls
{
	my ($self, $url) = @_;

	my $page = $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl());
	return () if (!$page);

	my @clearanceUrls = ();

	eval {
		@clearanceUrls = $page->dom
			->find(".hero__cta-zone a.hero__cta")	
			->map( sub {
				my $el = $_;
				my $href = $el->attr("href");

				 my $url = ($href =~ /^http/) ?
					$href :
					$self->getBaseUrl() . $href;

				return {
					"level3" => $el->text,
					"level3Url" => $url
				};
			})
			->each();
	};

	return @clearanceUrls;
}

sub getPagesUrls
{
	my ($self, $url) = @_;

	my $page = $self->getRedirectPage($url, "baseUrl" => $self->getBaseUrl());
	return () if (!$page);

	my $lastUrl = $self->getLastUrl();
	my @pagesUrls = ();

	eval {
		my $opts = $page->dom
			->at("a.view-all")
			->attr("href");

		if ($opts)
		{
			my $rawUrl = $lastUrl;
			if ($lastUrl =~ /\?/)
			{
				($rawUrl) = split(/\?/, $lastUrl);
			}
			@pagesUrls = ($rawUrl . $opts) if ($rawUrl);
		}
	};

	return (scalar(@pagesUrls) > 0) ? @pagesUrls : $lastUrl;
}

sub getProductsUrls
{
	my ($self, @pagesUrls) = @_;

	return () if (scalar(@pagesUrls) == 0);

	my @productsUrls = ();

	my ($pageUrl, $page, @tmpUrls);
	foreach $pageUrl (@pagesUrls)
	{
		debug("Handling page $pageUrl ...");

		$page = $self->getRedirectPage($pageUrl, "baseUrl" => $self->getBaseUrl());
		next if (!$page);

		@tmpUrls = ();
		eval {
			@tmpUrls = $page->dom
				->find("div.product-tile a")		
				->attr("href")
				->map( sub {
					return $_ =~ /^http/ ?
						$_ :
						$self->getBaseUrl() . $_
				})
				->each();
		};

		if (scalar(@tmpUrls) > 0)
		{
			push(@productsUrls, @tmpUrls);
		}
	}

	return @productsUrls;
}

# end links methods

# products methods

sub getTitle
{
	my ($self, $page) = @_;

	my $title = "";
	eval {
		$title = $page->dom
			->at(".main-content h1")
			->text();
	};

	return $title;
}

sub getImages
{
	my ($self, $page) = @_;

	my $images = [];

	eval {
		my @images = $page->dom
			->find('a.imageLinkThumb img')
			->attr("data-primaryimagesrc")
			->map( sub {
				s/^\/+/http:\/\//;
				$_;
			})
			->each();

		$images = [ @images ];
	};
	return $images;
}

sub getPricesAndColors
{
	my ($self, $page) = @_;

	my @colors;
	my $price = "0.0";
	my $salePrice;
	
	eval {
		my $label = $page->dom
			->at("#sizeAndAddToCart p.image.active")
			->parent();

		my $color = $label->at("p.title a")->text;

		push(@colors, $color) if ($color);

		my $pricesBlock = $label
			->at(".price-container > span.price");

		my $salesBlocks = $pricesBlock->find("span.sale");

		if ($salesBlocks->size > 0)
		{
			$price = filterPrice($pricesBlock->at(".strikethrough")->text);

			my $salesStr = join(" ", $salesBlocks->map( sub { $_->text; } )->each());
			$salePrice = filterPrice($salesStr);
		}
		else
		{
			$price = filterPrice($pricesBlock->at("a span")->text);
		}
	};

	return ($price, $salePrice, \@colors);
}

sub getSizes
{
	my ($self, $page) = @_;

	my @sizes = ();

	eval {
		@sizes = $page->dom
			->find("div.attribute-sizes")
			->map( sub {
				return $_->attr("class") =~ /unavailable/ ?
					undef :
					$_->at(".size")->text;
			})
			->each();

		@sizes = grep(defined, @sizes);
	};

	return \@sizes;
}

sub getDescription
{
	my ($self, $page) = @_;

	my $material;
	my $sole;

	my $description;
	my $contentAndCare;
	my $sizeAndFit;

	eval {
		my $info = $page->dom
			->at(".ProductDescription .description")
			->all_text;

		my @info = split(/ - /, $info);
		$description = shift(@info) if (scalar(@info) > 0);

		if (scalar(@info) > 0)
		{
			$sizeAndFit = " - " . join("\n - ", @info);
		}
	};

	eval {
		my $material = $page->dom
			->at(".ProductDescription .material")
			->text;

		$contentAndCare = $material if ($material);
	};

	eval {
		my $sole = $page->dom
			->at(".ProductDescription .sole")
			->text;

		if ($sole)
		{
			$contentAndCare = $contentAndCare ?
				$contentAndCare . "\n" . $sole :
				$sole;
		}
	};

	return ($description, $contentAndCare, $sizeAndFit);
}

# end products methods

sub generateMap
{
	my ($self, $file) = @_;

	my $struct = [];

	my $coreUrl = $self->getBaseUrl() . "ca/en";

	my $page = $self->getPage($coreUrl);

	my @parentCategories = $self->getParentCategories($page);
	$self->die("Could not load parent categories") if (!@parentCategories);

	my ($category, $subCategory, @subCategories,
		@pagesUrls, @productsUrls);

	foreach $category (@parentCategories)
	{
		my $categoryUrl = $category->{"level1Url"};
		debug("Handling Parent $categoryUrl ...");

		$page = $self->getRedirectPage($categoryUrl,
			"baseUrl" => $self->getBaseUrl());
		next if (!$page);

		@subCategories = $self->getSubCategories($page, $category->{"level1"});

		foreach $subCategory (@subCategories)
		{
			debug("Handling subcategory: " . $subCategory->{"level2"}
				. " => " . $subCategory->{"level3"}
				. " => " . $subCategory->{"level3Url"});

			# @pagesUrls = ($subCategory->{"name"} eq "Clearance") ?
			# 	$self->getClearancePagesUrls($subCategory->{"url"}) :
			# 	$self->getPagesUrls($subCategory->{"url"});

			@pagesUrls = $self->getPagesUrls($subCategory->{"level3Url"});
			@productsUrls = $self->getProductsUrls(@pagesUrls);

			my $subStruct = {
				"level1"       => $category->{"level1"},
				"level2"       => $subCategory->{"level2"},
				"level3"       => $subCategory->{"level3"},
				"level1Url"    => $category->{"level1Url"},
				"level2Url"    => $subCategory->{"level2Url"},
				"level3Url"    => $subCategory->{"level2Url"},
				"productsUrls" => [ @productsUrls ],
				"pages" => [ @pagesUrls ]
			};

			push(@$struct, $subStruct);
		}
	}

	return $struct;
}

sub getPid
{
	my ($self, $url) = @_;

	my ($pid) = $url =~ /\/([\d-]+)$/;
	return $pid;
}

sub getPidPattern
{
	my ($self, $pid) = @_;
	return qr/\/$pid$/;
}

sub categoriesCallback
{
	my ($self, $info) = @_;

	my $level1 = $info->{"level1"};
	my $level2 = $info->{"level2"};
	my $level3 = $info->{"level3"};
 	my $url = $info->{"productsUrls"}->[0];

	if (!$self->getProductField("parentName"))
	{
		$self->setProductField("parentName", $level1);
	}

	my $parentName = $self->getProductField("parentName");

	if (lc($level2) eq "new arrivals" || lc($level3) eq "new arrivals")
	{
		$self->setProductField("isNewArrival", 1);
	}

	my $line = $self->buildCategoriesLine($level2, $level3);
	if (lc($level1) eq "sale")
	{
		my $saleLine = $self->buildCategoriesLine($level1, $level2, $level3);
 		$self->addProductField("sSubCategories", $saleLine) if ($saleLine);
		$self->addProductField("mSubCategories", $saleLine) if ($saleLine);
	}

	if (lc($level2) eq "new arrivals")
	{
 		$self->addProductField("naSubCategories", $line) if ($line);
	}
	else
	{
		$self->setProductField("validUrl", $url) if (!$self->getProductField("validUrl"));
	}
	$self->addProductField("mSubCategories", $line) if ($line && $level1 eq $parentName);
}

sub getProductStruct
{
	my ($self, $url, $struct, $subStruct, $categoriesInfo) = @_;

	my $page = $self->getPage($url);

	if (!$page)
	{
		debug("Error load product $url");
		return undef;
	}

	my ($price, $salePrice, $colors) = $self->getPricesAndColors($page);
 	my $sale = ($salePrice && $salePrice <= $price) ? JSON::true : JSON::false;

 	my ($description, $contentAndCare, $sizeAndFit) = $self->getDescription($page);

	my %product = ();
	tie(%product, "Tie::IxHash");

 	%product= (
 		"Url"                    => $url,
 		"Store"                  => "Aldo Shoes",
 		"Title"                  => $self->getTitle($page),
 		"Brand"                  => "Aldo Shoes",
 		"Images"                 => $self->getImages($page),
 		"Price"                  => $price,
 		"SalePrice"              => $salePrice,
 		"Description"            => $description,
 		"ContentAndCare"         => $contentAndCare,
 		"SizeAndFit"             => $sizeAndFit,
 		"Sku"                    => undef,
 		"ProductId"              => $self->getProductField("pid"),
 		"Sizes"                  => $self->getSizes($page),
 		"Colours"                => $colors,
 		"Quantity"               => 0,
 		"SizingInfo"             => "http://www.aldoshoes.com/ca/en/customerService/sizeGuide",
 		"FreeShippingAmount"     => undef,
		"OnlineOnly"             => undef,
 		"ParentCategory"         => $self->getProductField("parentName"),
 		"SubCategory"            => $self->getMainSubCategoriesRef(),
		"NewArrivalsSubCategory" => $self->getNewArrivalSubCategoriesRef(),
		"FeatureSubCategory"     => $self->getFeatureSubCategoriesRef(),
		"SaleSubCategory"        => $self->getSaleSubCategoriesRef(),
 		"NewArrival"             => $self->isNewArrivalJSON(),
 		"Sale"                   => $sale,
		"Featured"               => $self->isFeatureJSON()
 	);

	return \%product;
}

1;
